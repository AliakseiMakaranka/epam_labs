﻿using Microsoft.EntityFrameworkCore;
using TicketShop.DataAccess.Models;

namespace TicketShop.DataAccess
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<City> Cities { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=ResaleDB_Aliaksei_Makaranka;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>().HasKey(userRole => new { userRole.RoleId, userRole.UserId });

            modelBuilder.Entity<UserRole>().
                HasOne(userRole => userRole.User).
                WithMany(user => user.UserRoles).HasForeignKey(userRole => userRole.UserId);
        }
    }
}
