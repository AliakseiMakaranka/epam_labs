﻿namespace TicketShop.DataAccess.Enums
{
    public enum LocalizationEnum
    {
        Ru,
        En,
        Be
    }
}
