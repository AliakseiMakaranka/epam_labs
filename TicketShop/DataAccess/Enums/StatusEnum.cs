﻿namespace TicketShop.DataAccess.Enums
{
    public enum StatusEnum
    {
        WaitingForConfirmation,
        Confirmed,
        Rejected,
        Selling
    }
}
