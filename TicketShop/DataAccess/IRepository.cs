﻿using System.Collections.Generic;

namespace TicketShop.DataAccess
{
    public interface IRepository<TEntity, in TKey>
        where TEntity : class
        where TKey : struct
    {
        IEnumerable<TEntity> GetItemList();
        TEntity GetItem(TKey id);

        void Create(TEntity newItem);
        void Update(TEntity newItem);

        void Delete(TKey id);
        void Save();
    }
}
