﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TicketShop.DataAccess;

namespace TicketShop.DataAccess.Migrations
{
    [DbContext(typeof(ApplicationContext))]
    partial class ApplicationContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TicketShop.DataAccess.Models.City", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Cities");
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.Event", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Banner")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("VenueInfoKey");

                    b.HasKey("Id");

                    b.HasIndex("VenueInfoKey");

                    b.ToTable("Events");
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BuyerInfoKey");

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("TicketInfoKey");

                    b.Property<string>("TrackNo")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("BuyerInfoKey");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.Ticket", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("EventInfoKey");

                    b.Property<decimal>("Price");

                    b.Property<int>("SellerInfoKey");

                    b.Property<string>("SellerNotes")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("EventInfoKey");

                    b.HasIndex("SellerInfoKey");

                    b.ToTable("Tickets");
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Localization")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("PhoneNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.UserRole", b =>
                {
                    b.Property<int>("RoleId");

                    b.Property<int>("UserId");

                    b.HasKey("RoleId", "UserId");

                    b.HasIndex("UserId");

                    b.ToTable("UserRoles");
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.Venue", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("CityInfoKey");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("CityInfoKey");

                    b.ToTable("Venues");
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.Event", b =>
                {
                    b.HasOne("TicketShop.DataAccess.Models.Venue", "Venue")
                        .WithMany()
                        .HasForeignKey("VenueInfoKey")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.Order", b =>
                {
                    b.HasOne("TicketShop.DataAccess.Models.User", "Buyer")
                        .WithMany("Orders")
                        .HasForeignKey("BuyerInfoKey")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.Ticket", b =>
                {
                    b.HasOne("TicketShop.DataAccess.Models.Event", "Event")
                        .WithMany("Tickets")
                        .HasForeignKey("EventInfoKey")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TicketShop.DataAccess.Models.User", "Seller")
                        .WithMany("Tickets")
                        .HasForeignKey("SellerInfoKey")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.UserRole", b =>
                {
                    b.HasOne("TicketShop.DataAccess.Models.Role", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TicketShop.DataAccess.Models.User", "User")
                        .WithMany("UserRoles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketShop.DataAccess.Models.Venue", b =>
                {
                    b.HasOne("TicketShop.DataAccess.Models.City", "City")
                        .WithMany()
                        .HasForeignKey("CityInfoKey")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
