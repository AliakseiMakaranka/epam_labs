﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketShop.DataAccess.Models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(50)]
        public string TrackNo { get; set; }

        [Required]
        public int BuyerInfoKey { get; set; }
        [ForeignKey("BuyerInfoKey")]
        public virtual User Buyer { get; set; }

        [Required]
        public int TicketInfoKey { get; set; }
    }
}
