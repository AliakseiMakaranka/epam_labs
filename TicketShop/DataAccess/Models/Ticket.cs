﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketShop.DataAccess.Models
{
    public class Ticket
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [StringLength(50)]
        public string SellerNotes { get; set; }

        [Required]
        public int EventInfoKey { get; set; }
        [ForeignKey("EventInfoKey")]
        public virtual Event Event { get; set; }

        [Required]
        public int SellerInfoKey { get; set; }
        [ForeignKey("SellerInfoKey")]
        public virtual User Seller { get; set; }
    }
}
