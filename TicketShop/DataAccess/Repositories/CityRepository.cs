﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TicketShop.DataAccess.Models;

namespace TicketShop.DataAccess.Repositories
{
    public class CityRepository : IRepository<City, int>
    {
        readonly ApplicationContext _dataContext;

        public CityRepository()
        {
            _dataContext = new ApplicationContext();
        }

        public IEnumerable<City> GetItemList()
        {
            var result = _dataContext.Cities;

            return result;
        }

        public City GetItem(int cityId)
        {
            var result = _dataContext.Cities.FirstOrDefault(city => city.Id == cityId);

            return result;
        }

        public void Create(City newCity)
        {
            _dataContext.Cities.Add(newCity);
        }

        public void Update(City newCity)
        {
            _dataContext.Entry(newCity).State = EntityState.Modified;
        }

        public void Delete(int cityId)
        {
            City foundedCity = _dataContext.Cities.FirstOrDefault(city => city.Id == cityId);
            if (foundedCity != null)
                _dataContext.Cities.Remove(foundedCity);
        }

        public void Save()
        {
            _dataContext.SaveChanges();
        }
    }
}
