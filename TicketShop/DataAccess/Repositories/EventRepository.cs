﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TicketShop.DataAccess.Models;

namespace TicketShop.DataAccess.Repositories
{
    public class EventRepository : IRepository<Event, int>
    {
        readonly ApplicationContext _dataContext;

        public EventRepository()
        {
            _dataContext = new ApplicationContext();
        }

        public IEnumerable<Event> GetItemList()
        {
            var result = _dataContext.Events.Include(@event => @event.Venue).ThenInclude(y => y.City);

            return result;
        }

        public Event GetItem(int eventId)
        {
            var result = _dataContext.Events.FirstOrDefault(item => item.Id == eventId);

            return result;
        }

        public void Create(Event newEvent)
        {
            _dataContext.Events.Add(newEvent);
        }

        public void Update(Event newEvent)
        {
            _dataContext.Entry(newEvent).State = EntityState.Modified;
        }

        public void Delete(int eventId)
        {
            Event foundedEvent = _dataContext.Events.FirstOrDefault(@event => @event.Id == eventId);
            if (foundedEvent != null)
                _dataContext.Events.Remove(foundedEvent);
        }

        public void Save()
        {
            _dataContext.SaveChanges();
        }
    }
}
