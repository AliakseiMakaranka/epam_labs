﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TicketShop.DataAccess.Models;

namespace TicketShop.DataAccess.Repositories
{
    public class OrderRepository : IRepository<Order, int>
    {
        readonly ApplicationContext _dataContext;

        public OrderRepository()
        {
            _dataContext = new ApplicationContext();
        }

        public IEnumerable<Order> GetItemList()
        {
            var result = _dataContext.Orders;

            return result;
        }

        public Order GetItem(int orderId)
        {
            var result = _dataContext.Orders.FirstOrDefault(order => order.Id == orderId);

            return result;
        }

        public void Create(Order newOrder)
        {
            _dataContext.Orders.Add(newOrder);
        }

        public void Update(Order newOrder)
        {
            _dataContext.Entry(newOrder).State = EntityState.Modified;
        }

        public void Delete(int orderId)
        {
            Order foundedOrder = _dataContext.Orders.FirstOrDefault(order => order.Id == orderId);
            if (foundedOrder != null)
                _dataContext.Orders.Remove(foundedOrder);
        }

        public void Save()
        {
            _dataContext.SaveChanges();
        }
    }
}
