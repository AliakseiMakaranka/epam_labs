﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TicketShop.DataAccess.Models;

namespace TicketShop.DataAccess.Repositories
{
    public class RoleRepository : IRepository<Role, int>
    {
        readonly ApplicationContext _dataContext;

        public RoleRepository()
        {
            _dataContext = new ApplicationContext();
        }

        public IEnumerable<Role> GetItemList()
        {
            var result = _dataContext.Roles;

            return result;
        }

        public Role GetItem(int roleId)
        {
            var result = _dataContext.Roles.FirstOrDefault(role => role.Id == roleId);

            return result;
        }

        public void Create(Role newRole)
        {
            _dataContext.Roles.Add(newRole);
        }

        public void Update(Role newRole)
        {
            _dataContext.Entry(newRole).State = EntityState.Modified;
        }

        public void Delete(int roleId)
        {
            Role foundedRole = _dataContext.Roles.FirstOrDefault(role => role.Id == roleId);
            if (foundedRole != null)
                _dataContext.Roles.Remove(foundedRole);
        }

        public void Save()
        {
            _dataContext.SaveChanges();
        }
    }
}
