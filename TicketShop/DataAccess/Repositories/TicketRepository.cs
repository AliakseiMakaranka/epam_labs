﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TicketShop.DataAccess.Models;

namespace TicketShop.DataAccess.Repositories
{
    public class TicketRepository : IRepository<Ticket, int>
    {
        readonly ApplicationContext _dataContext;

        public TicketRepository()
        {
            _dataContext = new ApplicationContext();
        }

        public IEnumerable<Ticket> GetItemList()
        {
            var result = _dataContext.Tickets.Include(ticket => ticket.Event).Include(ticket => ticket.Seller);

            return result;
        }

        public Ticket GetItem(int ticketId)
        {
            var result = _dataContext.Tickets.FirstOrDefault(item => item.Id == ticketId);

            return result;
        }

        public void Create(Ticket ticket)
        {
            _dataContext.Tickets.Add(ticket);
        }

        public void Update(Ticket newTicket)
        {
            _dataContext.Entry(newTicket).State = EntityState.Modified;
        }

        public void Delete(int ticketId)
        {
            Ticket foundedTicket = _dataContext.Tickets.FirstOrDefault(ticket => ticket.Id == ticketId);
            if (foundedTicket != null)
                _dataContext.Tickets.Remove(foundedTicket);
        }

        public void Save()
        {
            _dataContext.SaveChanges();
        }
    }
}
