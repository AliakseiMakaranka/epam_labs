﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TicketShop.DataAccess.Models;

namespace TicketShop.DataAccess.Repositories
{
    public class UserRepository : IRepository<User, int>
    {
        readonly ApplicationContext _dataContext;

        public UserRepository()
        {
            _dataContext = new ApplicationContext();
        }

        public IEnumerable<User> GetItemList()
        {
            var users = _dataContext.Users.Include(user => user.UserRoles).ThenInclude(userRole => userRole.Role);

            return users;
        }

        public User GetItem(int userId)
        {
            var result = _dataContext.Users.FirstOrDefault(user => user.Id == userId);

            return result;
        }

        public void Create(User newUser)
        {
            _dataContext.Users.Add(newUser);
        }

        public void Update(User newUser)
        {
            _dataContext.Entry(newUser).State = EntityState.Modified;
        }

        public void Delete(int userId)
        {
            User foundedUser = _dataContext.Users.FirstOrDefault(user => user.Id == userId);
            if (foundedUser != null)
                _dataContext.Users.Remove(foundedUser);
        }

        public void Save()
        {
            _dataContext.SaveChanges();
        }
    }
}
