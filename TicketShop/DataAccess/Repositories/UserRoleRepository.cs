﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TicketShop.DataAccess.Models;

namespace TicketShop.DataAccess.Repositories
{
    public class UserRoleRepository : IRepository<UserRole, UserRoleKey>
    {
        readonly ApplicationContext _dataContext;

        public UserRoleRepository()
        {
            _dataContext = new ApplicationContext();
        }

        public IEnumerable<UserRole> GetItemList()
        {
            var result = _dataContext.UserRoles.Include(userRole => userRole.Role);

            return result;
        }

        public UserRole GetItem(UserRoleKey keys)
        {
            var result =
                _dataContext.UserRoles.FirstOrDefault(
                    userRole => userRole.UserId == keys.UserId && userRole.RoleId == keys.RoleId);

            return result;
        }

        public void Create(UserRole newUserRole)
        {
            _dataContext.UserRoles.Add(newUserRole);
        }

        public void Update(UserRole newUserRole)
        {
            _dataContext.Entry(newUserRole).State = EntityState.Modified;
        }

        public void Delete(UserRoleKey keys)
        {
            UserRole foundedUserRole =
                _dataContext.UserRoles.FirstOrDefault(
                    userRole => userRole.UserId == keys.UserId && userRole.RoleId == keys.RoleId);
            if (foundedUserRole != null)
                _dataContext.UserRoles.Remove(foundedUserRole);
        }

        public void Save()
        {
            _dataContext.SaveChanges();
        }
    }
}

