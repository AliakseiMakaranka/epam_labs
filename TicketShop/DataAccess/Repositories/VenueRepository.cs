﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TicketShop.DataAccess.Models;

namespace TicketShop.DataAccess.Repositories
{
    public class VenueRepository : IRepository<Venue, int>
    {
        readonly ApplicationContext _dataContext;

        public VenueRepository()
        {
            _dataContext = new ApplicationContext();
        }

        public IEnumerable<Venue> GetItemList()
        {
            var result = _dataContext.Venues.Include(x => x.City);

            return result;
        }

        public Venue GetItem(int venueId)
        {
            var result = _dataContext.Venues.FirstOrDefault(venue => venue.Id == venueId);

            return result;
        }

        public void Create(Venue newVenue)
        {
            _dataContext.Venues.Add(newVenue);
        }

        public void Update(Venue newVenue)
        {
            _dataContext.Entry(newVenue).State = EntityState.Modified;
        }

        public void Delete(int venueId)
        {
            Venue foundedVenue = _dataContext.Venues.FirstOrDefault(venue => venue.Id == venueId);
            if (foundedVenue != null)
                _dataContext.Venues.Remove(foundedVenue);
        }

        public void Save()
        {
            _dataContext.SaveChanges();
        }
    }
}
