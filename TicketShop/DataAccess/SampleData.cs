﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using TicketShop.DataAccess.Enums;
using TicketShop.DataAccess.Models;

namespace TicketShop.DataAccess
{
    public static class SampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationContext>();

            if (!context.Roles.Any())
            {
                context.Roles.AddRange(
                    new Role { Name = Enum.GetName(typeof(UserRoleEnum), UserRoleEnum.User) },
                    new Role { Name = Enum.GetName(typeof(UserRoleEnum), UserRoleEnum.Admin) }
                );
                context.SaveChanges();
            }
            if (!context.Users.Any())
            {
                context.Users.AddRange(
                    new User
                    {
                        FirstName = "Evgenia",
                        LastName = "Vasilieve",
                        Localization = Enum.GetName(typeof(LocalizationEnum), LocalizationEnum.En),
                        Address = "User_adress",
                        PhoneNumber = "1111111",
                        Email = "User",
                        Password = "User"
                    },
                    new User
                    {
                        FirstName = "Alex",
                        LastName = "Tesla",
                        Localization = Enum.GetName(typeof(LocalizationEnum), LocalizationEnum.Ru),
                        Address = "Admin_adress",
                        PhoneNumber = "2222222",
                        Email = "Admin",
                        Password = "Admin"
                    },
                    new User
                    {
                        FirstName = "Ivan",
                        LastName = "Bikov",
                        Localization = Enum.GetName(typeof(LocalizationEnum), LocalizationEnum.Be),
                        Address = "Guest_adress",
                        PhoneNumber = "3333333",
                        Email = "Guest",
                        Password = "Guest"    //role=user
                    }
                );
                context.SaveChanges();
            }

            if (!context.UserRoles.Any())
            {
                context.UserRoles.AddRange(
                    new UserRole { UserId = 1, RoleId = (int)UserRoleEnum.User },
                    new UserRole { UserId = 2, RoleId = (int)UserRoleEnum.Admin },
                    new UserRole { UserId = 3, RoleId = (int)UserRoleEnum.User }
                );
                context.SaveChanges();
            }

            if (!context.Orders.Any())
            {
                context.Orders.AddRange(
                    new Order { Status = Enum.GetName(typeof(StatusEnum), StatusEnum.WaitingForConfirmation), BuyerInfoKey = 2, TicketInfoKey = 1 },
                    new Order { Status = Enum.GetName(typeof(StatusEnum), StatusEnum.Confirmed),              BuyerInfoKey = 3, TicketInfoKey = 2, TrackNo = "345252345" },

                    new Order { Status = Enum.GetName(typeof(StatusEnum), StatusEnum.Selling),                BuyerInfoKey = 1, TicketInfoKey = 3 },
                    new Order { Status = Enum.GetName(typeof(StatusEnum), StatusEnum.WaitingForConfirmation), BuyerInfoKey = 3, TicketInfoKey = 4 },

                    new Order { Status = Enum.GetName(typeof(StatusEnum), StatusEnum.Confirmed),              BuyerInfoKey = 2, TicketInfoKey = 5, TrackNo = "426683211" },
                    new Order { Status = Enum.GetName(typeof(StatusEnum), StatusEnum.Selling),                BuyerInfoKey = 3, TicketInfoKey = 6 },


                    new Order { Status = Enum.GetName(typeof(StatusEnum), StatusEnum.Rejected),               BuyerInfoKey = 1, TicketInfoKey = 7 },
                    new Order { Status = Enum.GetName(typeof(StatusEnum), StatusEnum.Selling),                BuyerInfoKey = 3, TicketInfoKey = 8 },

                    new Order { Status = Enum.GetName(typeof(StatusEnum), StatusEnum.Confirmed),              BuyerInfoKey = 1, TicketInfoKey = 9, TrackNo = "426683211" },
                    new Order { Status = Enum.GetName(typeof(StatusEnum), StatusEnum.Confirmed),              BuyerInfoKey = 2, TicketInfoKey = 10, TrackNo = "35711231" }
                );
                context.SaveChanges();
            }

            if (!context.Cities.Any())
            {
                context.Cities.AddRange(
                    new City { Name = "London" },
                    new City { Name = "Moscov" },
                    new City { Name = "Paris" },
                    new City { Name = "Gomel" },
                    new City { Name = "Minsk" },
                    new City { Name = "Vitebsk" },
                    new City { Name = "Brest" },
                    new City { Name = "Polock" },
                    new City { Name = "Rio" },
                    new City { Name = "Piter" }
                );
                context.SaveChanges();
            }

            if (!context.Venues.Any())
            {
                context.Venues.AddRange(
                    new Venue { Name = "Venue1",  Address = "adr1",  CityInfoKey = 1 },
                    new Venue { Name = "Venue2",  Address = "adr2",  CityInfoKey = 2 },
                    new Venue { Name = "Venue3",  Address = "adr3",  CityInfoKey = 3 },
                    new Venue { Name = "Venue4",  Address = "adr4",  CityInfoKey = 4 },
                    new Venue { Name = "Venue5",  Address = "adr5",  CityInfoKey = 5 },
                    new Venue { Name = "Venue6",  Address = "adr6",  CityInfoKey = 6 },
                    new Venue { Name = "Venue7",  Address = "adr7",  CityInfoKey = 7 },
                    new Venue { Name = "Venue8",  Address = "adr8",  CityInfoKey = 8 },
                    new Venue { Name = "Venue9",  Address = "adr9",  CityInfoKey = 9 },
                    new Venue { Name = "Venue10", Address = "adr10", CityInfoKey = 10 }
                );
                context.SaveChanges();
            }

            if (!context.Events.Any())
            {
                context.Events.AddRange(
                    new Event { Name = "London Festival",   Date = new DateTime(2017, 9, 11), Banner = "~/Images/1.jpg", Description = "event 1 description", VenueInfoKey = 1 },
                    new Event { Name = "Moscov festival",   Date = new DateTime(2017, 10, 14), Banner = "~/Images/2.jpg", Description = "event 2 description", VenueInfoKey = 2 },
                    new Event { Name = "Paris Festival",    Date = new DateTime(2017, 11, 15), Banner = "~/Images/3.jpg", Description = "event 3 description", VenueInfoKey = 3 },

                    new Event { Name = "Gomel Festival",    Date = new DateTime(2017, 10, 26),  Banner = "~/Images/4.jpg", Description = "event 4 description", VenueInfoKey = 4 },
                    new Event { Name = "Minsk Festival",    Date = new DateTime(2017, 11, 27), Banner = "~/Images/5.jpg", Description = "event 5 description", VenueInfoKey = 5 },
                    new Event { Name = "Vitebsks Festival", Date = new DateTime(2017, 12, 28), Banner = "~/Images/6.jpg", Description = "event 6 description", VenueInfoKey = 6 },

                    new Event { Name = "Brest Festival",    Date = new DateTime(2018, 01, 11), Banner = "~/Images/7.jpg", Description = "event 7 description", VenueInfoKey = 7 },
                    new Event { Name = "Polock Festival",   Date = new DateTime(2018, 02, 12), Banner = "~/Images/8.jpg", Description = "event 8 description", VenueInfoKey = 8 },
                    new Event { Name = "Rio Festival",      Date = new DateTime(2018, 03, 23), Banner = "~/Images/9.jpg", Description = "event 9 description", VenueInfoKey = 9 }
                );
                context.SaveChanges();
            }

            if (!context.Tickets.Any())
            {
                context.Tickets.AddRange(
                    new Ticket { Price = 100, SellerInfoKey = 1, EventInfoKey = 1 },
                    new Ticket { Price = 200, SellerInfoKey = 1, EventInfoKey = 1 },

                    new Ticket { Price = 400, SellerInfoKey = 2, EventInfoKey = 2 },
                    new Ticket { Price = 500, SellerInfoKey = 2, EventInfoKey = 2 },

                    new Ticket { Price = 600, SellerInfoKey = 3, EventInfoKey = 3 },
                    new Ticket { Price = 800, SellerInfoKey = 3, EventInfoKey = 3 },

                    new Ticket { Price = 900, SellerInfoKey = 1, EventInfoKey = 1, SellerNotes = "Rejected by seller" },
                    new Ticket { Price = 1000, SellerInfoKey = 3, EventInfoKey = 3, SellerNotes = "Rejected by seller" },

                    new Ticket { Price = 1100, SellerInfoKey = 2, EventInfoKey = 3 },
                    new Ticket { Price = 1200, SellerInfoKey = 3, EventInfoKey = 1 }
                );
                context.SaveChanges();
            }
        }
    }
}