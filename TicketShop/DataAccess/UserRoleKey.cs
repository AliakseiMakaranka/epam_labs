﻿namespace TicketShop.DataAccess
{
    public struct UserRoleKey
    {
        public int UserId { get; set; }

        public int RoleId { get; set; }
    }
}
