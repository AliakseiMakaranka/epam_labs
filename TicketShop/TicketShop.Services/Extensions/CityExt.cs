﻿using System.Collections.Generic;
using TicketShop.DataAccess.Models;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Extensions
{
    public static class CityExt
    {
        //lists
        public static List<CityDto> Mapper(this List<City> cityType, List<City> cities)
        {
            List<CityDto> result = new List<CityDto>();

            foreach (var city in cities)
            {
                CityDto cityDto = new CityDto
                {
                    Id = city.Id,
                    Name = city.Name
                };

                result.Add(cityDto);
            }

            return result;
        }

        //Repeat for one
        public static CityDto Mapper(this City cityType, City city)
        {
            CityDto result = new CityDto
            {
                Id = city.Id,
                Name = city.Name
            };

            return result;
        }

        public static City Mapper(this CityDto cityDtoType, CityDto cityDto)
        {
            City result = new City
            {
                Id = cityDto.Id,
                Name = cityDto.Name
            };

            return result;
        }
    }
}
