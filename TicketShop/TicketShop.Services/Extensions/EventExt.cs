﻿using System.Collections.Generic;
using TicketShop.DataAccess.Models;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Extensions
{
    public static class EventExt
    {
        //lists
        public static List<EventDto> Mapper(this List<Event> eventType, List<Event> events)
        {
            List<EventDto> result = new List<EventDto>();

            foreach (var @event in events)
            {
                EventDto eventDto = new EventDto
                {
                    Id = @event.Id,
                    Name = @event.Name,
                    Banner = @event.Banner,
                    Description = @event.Description,
                    Date = @event.Date,
                    VenueInfoKey = @event.VenueInfoKey,
                    Venue = new VenueDto
                    {
                        Id = @event.Venue.Id,
                        Name = @event.Venue.Name,
                        Address = @event.Venue.Address,
                        CityInfoKey = @event.Venue.CityInfoKey,
                        City = new CityDto
                        {
                            Id = @event.Venue.City.Id,
                            Name = @event.Venue.City.Name
                        }
                    }
                };

                result.Add(eventDto);
            }

            return result;
        }

        //Repeat for one
        public static EventDto Mapper(this Event eventType, Event @event)
        {
            EventDto result = new EventDto
            {
                Id = @event.Id,
                Name = @event.Name,
                Banner = @event.Banner,
                Date = @event.Date,
                Description = @event.Description,
                VenueInfoKey = @event.VenueInfoKey
            };

            return result;
        }

        public static Event Mapper(this EventDto eventDtoType, EventDto eventDto)
        {
            Event result = new Event
            {
                Id = eventDto.Id,
                Name = eventDto.Name,
                Banner = eventDto.Banner,
                Date = eventDto.Date,
                Description = eventDto.Description,
                VenueInfoKey = eventDto.VenueInfoKey
            };

            return result;
        }
    }
}
