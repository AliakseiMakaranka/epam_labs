﻿using System.Collections.Generic;
using TicketShop.DataAccess.Models;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Extensions
{
    public static class OrderExt
    {
        //lists
        public static List<OrderDto> Mapper(this List<Order> orderType, List<Order> orders)
        {
            var result = new List<OrderDto>();

            foreach (var order in orders)
            {
                if (order != null)
                {
                    OrderDto orderDto = order.Mapper(order);
                    result.Add(orderDto);
                }
            }

            return result;
        }

        //Repeat for one
        public static OrderDto Mapper(this Order orderType, Order order)
        {
            OrderDto result = new OrderDto
            {
                Id = order.Id,
                Status = order.Status,
                BuyerInfoKey = order.BuyerInfoKey,
                TrackNo = order.TrackNo,

                TicketInfoKey = order.TicketInfoKey,
                Buyer = order.Buyer?.Mapper(order.Buyer)
            };

            return result;
        }
    }
}
