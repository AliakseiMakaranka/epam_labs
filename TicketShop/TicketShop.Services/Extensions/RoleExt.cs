﻿using System.Collections.Generic;
using TicketShop.DataAccess.Models;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Extensions
{
    public static class RoleExt
    {
        //lists
        public static List<RoleDto> Mapper(this List<Role> roleType, List<Role> roles)
        {
            List<RoleDto> result = new List<RoleDto>();

            foreach (var role in roles)
            {
                RoleDto roleDto = new RoleDto
                {
                    Id = role.Id,
                    Name = role.Name
                };

                result.Add(roleDto);
            }

            return result;
        }

        //Repeat for one
        public static RoleDto Mapper(this Role roleType, Role role)
        {
            RoleDto result = new RoleDto
            {
                Id = role.Id,
                Name = role.Name
            };

            return result;
        }
    }
}
