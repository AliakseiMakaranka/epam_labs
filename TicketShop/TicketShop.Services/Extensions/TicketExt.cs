﻿using System.Collections.Generic;
using TicketShop.DataAccess.Models;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Extensions
{
    public static class TicketExt
    {
        //lists
        public static List<TicketDto> Mapper(this List<Ticket> ticketType, List<Ticket> tickets)
        {
            List<TicketDto> result = new List<TicketDto>();

            foreach (var ticket in tickets)
            {
                TicketDto ticketDto = new TicketDto
                {
                    Id = ticket.Id,
                    Price = ticket.Price,
                    SellerNotes = ticket.SellerNotes,
                    EventInfoKey = ticket.EventInfoKey,
                    SellerInfoKey = ticket.SellerInfoKey,
                    Event = new EventDto
                    {
                        Description = ticket.Event.Description,
                        Id = ticket.Event.Id,
                        Name = ticket.Event.Name,
                        Banner = ticket.Event.Banner,
                        Date = ticket.Event.Date,
                        VenueInfoKey = ticket.Event.VenueInfoKey
                    },
                    Seller = new UserDto
                    {
                        FirstName = ticket.Seller.FirstName,
                        LastName = ticket.Seller.LastName,
                        Email = ticket.Seller.Email,
                        PhoneNumber = ticket.Seller.PhoneNumber
                    }
                   
                    
                };

                result.Add(ticketDto);
            }

            return result;
        }

        //Repeat for one
        public static TicketDto Mapper(this Ticket ticketType, Ticket ticket)
        {
            TicketDto result;

            if (ticket.Event!= null)
            {
                result = new TicketDto
                {
                    Id = ticket.Id,
                    Price = ticket.Price,
                    SellerNotes = ticket.SellerNotes,
                    EventInfoKey = ticket.EventInfoKey,
                    SellerInfoKey = ticket.SellerInfoKey,
                    Event = new EventDto
                    {
                        Description = ticket.Event.Description,
                        Id = ticket.Event.Id,
                        Name = ticket.Event.Name,
                        Banner = ticket.Event.Banner,
                        Date = ticket.Event.Date,
                        VenueInfoKey = ticket.Event.VenueInfoKey
                    },
                    Seller = new UserDto
                    {
                        FirstName = ticket.Seller.FirstName,
                        LastName = ticket.Seller.LastName,
                        Email = ticket.Seller.Email,
                        PhoneNumber = ticket.Seller.PhoneNumber
                    }
                };
            }
            else
            {
                result = new TicketDto
                {
                    Id = ticket.Id,
                    EventInfoKey = ticket.EventInfoKey,
                    SellerInfoKey = ticket.SellerInfoKey,
                    SellerNotes = ticket.SellerNotes,
                    Price = ticket.Price
                };
            }
            
            return result;
        }

        public static Ticket Mapper(this TicketDto ticketDtoType, TicketDto ticketDto)
        {
            Ticket result = new Ticket
            {
                Id = ticketDto.Id,
                EventInfoKey = ticketDto.EventInfoKey,
                SellerInfoKey = ticketDto.SellerInfoKey,
                SellerNotes = ticketDto.SellerNotes,
                Price = ticketDto.Price
            };

            return result;
        }
    }
}
