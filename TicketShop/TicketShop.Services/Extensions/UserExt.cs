﻿using TicketShop.DataAccess.Models;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Extensions
{
    public static class UserExt
    {
       //Repeat for one
        public static UserDto Mapper(this User userType, User user)
        {
            var result = new UserDto
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Address = user.Address,
                Localization = user.Localization,
                PhoneNumber = user.PhoneNumber
            };

            return result;
        }
    }
}
