﻿using System.Collections.Generic;
using TicketShop.DataAccess.Models;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Extensions
{
    public static class UserRoleExt
    {
        //lists
        public static List<UserRoleDto> Mapper(this List<UserRole> userRoleType, List<UserRole> userRoles)
        {
            List<UserRoleDto> result = new List<UserRoleDto>();

            foreach (var userRole in userRoles)
            {
                UserRoleDto userRoleDto = new UserRoleDto
                {
                    RoleId = userRole.RoleId,
                    UserId = userRole.UserId,
                    Role = new RoleDto
                    {
                        Id = userRole.Role.Id,
                        Name = userRole.Role.Name
                    }
                };

                result.Add(userRoleDto);
            }

            return result;
        }

        //Repeat for one
        public static UserRoleDto Mapper(this UserRole userRoleType, UserRole userRole)
        {
            UserRoleDto result = new UserRoleDto
            {
                RoleId = userRole.RoleId,
                UserId = userRole.UserId,

                Role = new RoleDto
                {
                    Id = userRole.Role.Id,
                    Name = userRole.Role.Name
                }
            };

            return result;
        }

        public static UserRole Mapper(this UserRoleDto userRoleDtoType, UserRoleDto userRoleDto)
        {
            UserRole result = new UserRole
            {
                RoleId = userRoleDto.RoleId,
                UserId = userRoleDto.UserId
            };

            return result;
        }
    }
}
