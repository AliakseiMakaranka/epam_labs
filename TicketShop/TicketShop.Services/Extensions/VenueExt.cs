﻿using System.Collections.Generic;
using TicketShop.DataAccess.Models;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Extensions
{
    public static class VenueExt
    {
        //lists
        public static List<VenueDto> Mapper(this List<Venue> venueType, List<Venue> venues)
        {
            List<VenueDto> result = new List<VenueDto>();

            foreach (var venue in venues)
            {
                VenueDto venueDto = new VenueDto
                {
                    Id = venue.Id,
                    Name = venue.Name,
                    Address = venue.Address,
                    CityInfoKey = venue.CityInfoKey,
                    City = new CityDto
                    {
                        Id = venue.City.Id,
                        Name = venue.City.Name
                    }
                };

                result.Add(venueDto);
            }

            return result;
        }

        //Repeat for one
        public static VenueDto Mapper(this Venue venueType, Venue venue)
        {
            VenueDto result = new VenueDto
            {
                Id = venue.Id,
                Name = venue.Name,
                Address = venue.Address,
                CityInfoKey = venue.CityInfoKey
            };

            return result;
        }

        public static Venue Mapper(this VenueDto venueDtoType, VenueDto venueDto)
        {
            Venue result = new Venue
            {
                Id = venueDto.Id,
                Name = venueDto.Name,
                Address = venueDto.Address,
                CityInfoKey = venueDto.CityInfoKey
            };

            return result;
        }
    }
}
