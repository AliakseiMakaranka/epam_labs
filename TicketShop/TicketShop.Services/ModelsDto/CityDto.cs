﻿namespace TicketShop.Services.ModelsDto
{
    public class CityDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}