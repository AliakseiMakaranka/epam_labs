﻿using System;
using System.Collections.Generic;

namespace TicketShop.Services.ModelsDto

{
    public class EventDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public string Banner { get; set; }

        public string Description { get; set; }

        public int VenueInfoKey { get; set; }

        public VenueDto Venue { get; set; }

        public List<TicketDto> Tickets { get; set; }
    }
}
