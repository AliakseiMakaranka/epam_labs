﻿namespace TicketShop.Services.ModelsDto
{
    public class OrderDto
    {
        public int Id { get; set; }

        public string Status { get; set; }

        public string TrackNo { get; set; }

        public int BuyerInfoKey { get; set; }

        public int TicketInfoKey { get; set; }

        public UserDto Buyer { get; set; }
    }
}
