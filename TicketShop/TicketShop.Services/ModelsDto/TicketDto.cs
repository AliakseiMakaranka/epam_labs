﻿namespace TicketShop.Services.ModelsDto
{
    public class TicketDto
    {
        public int Id { get; set; }

        public decimal Price { get; set; }

        public string SellerNotes { get; set; }

        public int EventInfoKey { get; set; }

        public EventDto Event { get; set; }

        public int SellerInfoKey { get; set; }

        public UserDto Seller { get; set; }
    }
}
