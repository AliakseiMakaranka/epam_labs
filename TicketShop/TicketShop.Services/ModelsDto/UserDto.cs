﻿using System.Collections.Generic;

namespace TicketShop.Services.ModelsDto
{
    public class UserDto
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Localization { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public List<UserRoleDto> UserRoles { get; set; }

        public List<OrderDto> Orders { get; set; }

        public List<TicketDto> Tickets { get; set; }
    }
}