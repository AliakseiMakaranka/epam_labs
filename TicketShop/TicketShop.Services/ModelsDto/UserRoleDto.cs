﻿namespace TicketShop.Services.ModelsDto
{
    public class UserRoleDto
    {
        public int UserId { get; set; }

        public UserDto User { get; set; }

        public int RoleId { get; set; }

        public RoleDto Role { get; set; }
    }
}
