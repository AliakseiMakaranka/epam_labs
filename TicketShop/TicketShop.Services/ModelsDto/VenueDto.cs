﻿namespace TicketShop.Services.ModelsDto
{
    public class VenueDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public int CityInfoKey { get; set; }

        public CityDto City { get; set; }
    }
}
