﻿using System.Collections.Generic;
using System.Linq;
using TicketShop.DataAccess;
using TicketShop.DataAccess.Models;
using TicketShop.Services.Extensions;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;

namespace TicketShop.Services.Services
{
    public class CityService : ICityService
    {
        readonly IRepository<City, int> _citiesRepository;

        public CityService(IRepository<City, int> cityRepository)
        {
            _citiesRepository = cityRepository;
        }

        public List<CityDto> GetCities()
        {
            List<City> cities = _citiesRepository.GetItemList().ToList();
            var result = cities.Mapper(cities);

            return result;
        }

        public CityDto GetCity(int cityId)
        {
            City city = _citiesRepository.GetItem(cityId);
            var result = city.Mapper(city);

            return result;
        }

        public CityDto GetCity(string cityName)
        {
            City city = _citiesRepository.GetItemList().FirstOrDefault(x => x.Name == cityName);
            var result = city.Mapper(city);
            
            return result;
        }

        public void Create(CityDto cityDto)
        {
            var result = cityDto.Mapper(cityDto);

            _citiesRepository.Create(result);
            _citiesRepository.Save();
        }

        public void Edit(CityDto cityDto)
        {
            var result = cityDto.Mapper(cityDto);
            
            _citiesRepository.Update(result);
            _citiesRepository.Save();
        }

        public void Delete(CityDto cityDto)
        {
            _citiesRepository.Delete(cityDto.Id);
            _citiesRepository.Save();
        }
    }
}
