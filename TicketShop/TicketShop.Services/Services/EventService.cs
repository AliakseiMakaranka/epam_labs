﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketShop.DataAccess;
using TicketShop.DataAccess.Models;
using TicketShop.Services.Extensions;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;

namespace TicketShop.Services.Services
{
    public class EventService : IEventService
    {
        readonly IRepository<Event, int> _eventsRepository;
        readonly IRepository<City, int> _cityRepository;
        readonly IRepository<Venue, int> _venueRepository;

        public EventService(
            IRepository<Event, int> eventRepository,
            IRepository<City, int> cityRepository,
            IRepository<Venue, int> venueRepository)
        {
            _eventsRepository = eventRepository;
            _cityRepository = cityRepository;
            _venueRepository = venueRepository;
        }

        public List<EventDto> GetActualDateEvents()
        {
            List<Event> events = _eventsRepository.GetItemList()
                .Where(date => date.Date.CompareTo(DateTime.Now) > 0).ToList();

            foreach (var @event in events)
            {
                @event.Venue.City = _cityRepository.GetItem(@event.Venue.CityInfoKey);
            }

            var result = events.Mapper(events);

            return result;
        }

        public EventDto GetEvent(int eventId)
        {
            Event @event = _eventsRepository.GetItem(eventId);
            var result = @event.Mapper(@event);

            return result;
        }

        public EventDto GetEvent(string eventName)
        {
            Event @event = _eventsRepository.GetItemList().FirstOrDefault(x => x.Name == eventName);

            return @event?.Mapper(@event);
        }

        public List<EventDto> GetEventOverlap(
            string eventName,
            string beginDate,
            string endDate,
            int[] venuesIds,
            int[] citiesIds,
            int pageSize,
            int pageNumber)
        {
            //берем только актуальные по дате ивенты
            List<Event> events = _eventsRepository.GetItemList()
                .Where(date => date.Date.CompareTo(DateTime.Now) > 0).ToList();

            foreach (var @event in events)
            {
                @event.Venue.City = _cityRepository.GetItem(@event.Venue.CityInfoKey);
            }

            List<Event> temp;

            if (eventName != null)
            {
                temp = events;
                events = temp.Where(x => x.Name.Contains(eventName)).ToList();
            }

            if (beginDate != null)
            {
                temp = events;
                DateTime _beginDate = DateTime.ParseExact(beginDate, "MM/dd/yyyy", null);

                events = temp.Where(o => o.Date >= Convert.ToDateTime(_beginDate)).ToList();
            }

            if (endDate != null)
            {
                temp = events;
                DateTime _endDate = DateTime.ParseExact(endDate, "MM/dd/yyyy", null);

                events = temp.Where(o => o.Date <= Convert.ToDateTime(_endDate)).ToList();
            }

            if (citiesIds.Length != 0)
            {
                temp = events;
                List<Event> eventFilterCities = new List<Event>();
                List<Venue> venues = new List<Venue>();

                foreach (var citiesId in citiesIds)
                {
                    venues.AddRange(_venueRepository.GetItemList().Where(x => x.CityInfoKey == citiesId));
                }

                foreach (var venue in venues)
                {
                    eventFilterCities.AddRange(temp.Where(x => x.VenueInfoKey == venue.Id));
                }

                events = eventFilterCities;
            }

            if (venuesIds.Length != 0)
            {
                temp = events;
                List<Event> eventFilterVenues = new List<Event>();

                foreach (var venuesId in venuesIds)
                {
                    eventFilterVenues.AddRange(temp.Where(x => x.VenueInfoKey == venuesId));
                }
                events = eventFilterVenues;
            }

            var result = events.Mapper(events);

            return result;
        }

        public List<EventDto> GetEvents()
        {
            List<Event> events = _eventsRepository.GetItemList().ToList();
            var result = events.Mapper(events);

            return result;
        }

        public void Create(EventDto eventDto)
        {
            var result = eventDto.Mapper(eventDto);

            _eventsRepository.Create(result);
            _eventsRepository.Save();
        }

        public void Edit(EventDto eventDto)
        {
            var result = eventDto.Mapper(eventDto);

            _eventsRepository.Update(result);
            _eventsRepository.Save();
        }

        public void Delete(EventDto eventDto)
        {
            _eventsRepository.Delete(eventDto.Id);
            _eventsRepository.Save();
        }
    }
}
