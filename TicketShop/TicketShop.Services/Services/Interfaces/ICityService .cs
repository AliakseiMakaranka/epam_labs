﻿using System.Collections.Generic;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Services.Interfaces
{
    public interface ICityService
    {
        List<CityDto> GetCities();

        CityDto GetCity(int cityId);

        CityDto GetCity(string cityName);

        void Create(CityDto cityDto);

        void Edit(CityDto cityDto);

        void Delete(CityDto cityDto);
    }
}
