﻿using System.Collections.Generic;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Services.Interfaces
{
    public interface IEventService
    {
        List<EventDto> GetEvents();

        List<EventDto> GetActualDateEvents();

        EventDto GetEvent(int eventId);

        EventDto GetEvent(string eventName);

        List<EventDto> GetEventOverlap(string eventName, string beginDate, string endDate, int[] venuesIds, int[] citiesIds, int pageSize, int pageNumber);

        void Create(EventDto eventDto);

        void Edit(EventDto eventDto);

        void Delete(EventDto eventDto);
    }
}
