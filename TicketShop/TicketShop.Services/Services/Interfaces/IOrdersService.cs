﻿using System.Collections.Generic;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Services.Interfaces
{
    public interface IOrdersService
    {
        List<OrderDto> GetOrders();

        OrderDto GetOrder(int orderId);

        List<OrderDto> GetOrdersByUserName(string currentUserName);

        void AddToOrder(int buyerId, int ticketId);

        void Confirm(int ticketId, string trackNo);

        void Reject(int ticketId, string comment);
    }
}
