﻿using System.Collections.Generic;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Services.Interfaces
{
    public interface IRoleService
    {
        List<RoleDto> GetRoles();

        RoleDto GetRole(int roleId);

        RoleDto GetRole(string roleName);
    }
}
