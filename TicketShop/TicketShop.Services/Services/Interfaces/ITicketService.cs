﻿using System.Collections.Generic;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Services.Interfaces
{
    public interface ITicketService
    {
        List<TicketDto> GetTickets();

        List<TicketDto> GetSellingTicketsOfEvent(int eventId);

        List<TicketDto> GetTicketByEventId(int eventId);

        TicketDto GetTicket(int ticketId);

        List<TicketDto> GetTicketsByUserName(string currentUserName);

        void Create(TicketDto ticketDto);
    }
}
