﻿using System.Collections.Generic;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Services.Interfaces
{
    public interface IUserRoleService
    {
        List<UserRoleDto> GetUserRoles();

        UserRoleDto GetUserRoleByUserName(string userEmail);

        UserRoleDto GetUserRoleByRoleName(string roleName);

        void Create(UserRoleDto userRoleDto);

        void Delete(UserRoleDto userRoleDto);

        List<UserRoleDto> GetUserRolesByUserId(int userId);
    }
}
