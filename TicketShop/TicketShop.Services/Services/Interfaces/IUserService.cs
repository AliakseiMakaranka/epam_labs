﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Services.Interfaces
{
    public interface IUserService
    {
        List<UserDto> GetUsers();

        UserDto GetUser(int userId);

        UserDto GetUser(string email);

        void Create(UserDto userDto);

        SelectList GetLanguageList();

        void Edit(UserDto userDto);

        void Delete(UserDto userDto);
    }
}
