﻿using System.Collections.Generic;
using TicketShop.Services.ModelsDto;

namespace TicketShop.Services.Services.Interfaces
{
    public interface IVenueService
    {
        List<VenueDto> GetVenues();

        VenueDto GetVenue(int venueId);

        VenueDto GetVenue(string venueName);

        void Create(VenueDto venueDto);

        void Edit(VenueDto venueDto);

        void Delete(VenueDto venueDto);
    }
}
