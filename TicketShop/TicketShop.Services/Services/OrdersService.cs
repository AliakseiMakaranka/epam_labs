﻿using System.Collections.Generic;
using System.Linq;
using TicketShop.DataAccess;
using TicketShop.DataAccess.Enums;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;
using TicketShop.DataAccess.Models;
using TicketShop.Services.Extensions;

namespace TicketShop.Services.Services
{
    public class OrdersService : IOrdersService
    {
        readonly IRepository<User, int> _userRepository;
        readonly IRepository<Order, int> _orderRepository;
        readonly IRepository<Ticket, int> _ticketRepository;

        public OrdersService(
            IRepository<User, int> userRepository,
            IRepository<Order, int> orderRepository,
            IRepository<Ticket, int> ticketRepositor
            )
        {
            _userRepository = userRepository;
            _orderRepository = orderRepository;
            _ticketRepository = ticketRepositor;
        }

        public List<OrderDto> GetOrders()
        {
            IEnumerable<Order> orders = _orderRepository.GetItemList();
            List<OrderDto> result = new List<OrderDto>();

            foreach (var order in orders)
            {
                order.Buyer = _userRepository.GetItemList().FirstOrDefault(x => x.Id == order.BuyerInfoKey); 
                OrderDto orderDto = order.Mapper(order);
                
                result.Add(orderDto);
            }

            return result;
        }

        public OrderDto GetOrder(int orderId)
        {
            Order order = _orderRepository.GetItem(orderId);
            var result = order.Mapper(order);

            return result;
        }

        public List<OrderDto> GetOrdersByUserName(string currentUserName)
        {
            User currentUser = _userRepository.GetItemList().FirstOrDefault(user => user.Email == currentUserName);
            List<Order> orders = _orderRepository.GetItemList().Where(x => x.BuyerInfoKey == currentUser.Id).ToList();
            var result = orders.Mapper(orders);

            return result;
        }

        public void AddToOrder(int buyerId, int ticketId)
        {
            Ticket ticket = _ticketRepository.GetItemList().FirstOrDefault(x => x.Id == ticketId);

            Order ticketOrder = _orderRepository.GetItemList().FirstOrDefault(x => x.TicketInfoKey == ticketId);

            if (ticketOrder == null)
            {
                Order order = new Order
                {
                    Status = StatusEnum.WaitingForConfirmation.ToString(),
                    TicketInfoKey = ticket.Id,
                    BuyerInfoKey = buyerId
                };
                _orderRepository.Create(order);
            }
            else
            {
                ticketOrder.Status = StatusEnum.WaitingForConfirmation.ToString();
                _orderRepository.Update(ticketOrder);
            }

            _orderRepository.Save();
        }

        public void Confirm(int ticketId, string trackNo)
        {
            Order order = _orderRepository.GetItemList().FirstOrDefault(x => x.TicketInfoKey == ticketId);
            order.Status = StatusEnum.Confirmed.ToString();
            order.TrackNo = trackNo;

            _orderRepository.Update(order);
            _orderRepository.Save();
        }

        public void Reject(int ticketId, string comment)
        {
            Order order = _orderRepository.GetItemList().FirstOrDefault(x => x.TicketInfoKey == ticketId);
            Ticket ticket = _ticketRepository.GetItem(ticketId);
            ticket.SellerNotes = comment;

            order.Status = StatusEnum.Rejected.ToString();

            _ticketRepository.Update(ticket);
            _ticketRepository.Save();

            _orderRepository.Update(order);
            _orderRepository.Save();
        }
    }
}
