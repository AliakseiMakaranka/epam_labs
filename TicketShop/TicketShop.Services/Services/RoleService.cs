﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketShop.DataAccess;
using TicketShop.DataAccess.Enums;
using TicketShop.DataAccess.Models;
using TicketShop.Services.Extensions;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;

namespace TicketShop.Services.Services
{
    public class RoleService : IRoleService
    {
        readonly IRepository<Role, int> _roleRepository;

        public RoleService(IRepository<Role, int> roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public List<RoleDto> GetRoles()
        {
            List<Role> roles = _roleRepository.GetItemList().ToList();
            var result = roles.Mapper(roles);

            return result;
        }

        public RoleDto GetRole(int roleId)
        {
            Role role = _roleRepository.GetItem(roleId);
            var result = role.Mapper(role);

            return result;
        }

        public RoleDto GetRole(string roleName)
        {
            Role role = _roleRepository.GetItemList()
                    .FirstOrDefault(name => Enum.GetName(typeof(UserRoleEnum), name.Name) == roleName);

            var result = role.Mapper(role);
            
            return result;
        }
    }
}
