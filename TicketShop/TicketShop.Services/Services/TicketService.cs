﻿using System.Collections.Generic;
using System.Linq;
using TicketShop.DataAccess;
using TicketShop.DataAccess.Enums;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;
using TicketShop.DataAccess.Models;
using TicketShop.Services.Extensions;

namespace TicketShop.Services.Services
{
    public class TicketService : ITicketService
    {
        readonly IRepository<User, int> _userRepository;
        readonly IRepository<Ticket, int> _ticketRepository;
        readonly IRepository<Order, int> _orderRepository;

        public TicketService(
            IRepository<User, int> userRepository,
            IRepository<Ticket, int> ticketRepository,
            IRepository<Order, int> orderRepository
        )
        {
            _userRepository = userRepository;
            _ticketRepository = ticketRepository;
            _orderRepository = orderRepository;
        }

        public List<TicketDto> GetTickets()
        {
            List<Ticket> tickets = _ticketRepository.GetItemList().ToList();
            var result = tickets.Mapper(tickets);

            return result;
        }

        public TicketDto GetTicket(int ticketId)
        {
            Ticket ticket = _ticketRepository.GetItem(ticketId);
            var result = ticket.Mapper(ticket);

            return result;
        }

        public List<TicketDto> GetTicketByEventId(int eventId)
        {
            var tickets = _ticketRepository.GetItemList().Where(ticket => ticket.EventInfoKey == eventId).ToList();
            var result = tickets.Mapper(tickets);
            
            return result;
        }

        public List<TicketDto> GetTicketsByUserName(string currentUserName)
        {
            User currentUser = _userRepository.GetItemList().FirstOrDefault(user => user.Email == currentUserName);
            List<Ticket> tickets = _ticketRepository.GetItemList().Where(ticket => ticket.SellerInfoKey == currentUser.Id).ToList();
            List<TicketDto> result = tickets.Mapper(tickets);

            return result;
        }
        
        public void Create(TicketDto ticketDto)
        {
            var result = ticketDto.Mapper(ticketDto);
            
            _ticketRepository.Create(result);
            _ticketRepository.Save();
        }

        public List<TicketDto> GetSellingTicketsOfEvent(int eventId)
        {
            //выбираем все билеты на данный ивент, но без тех, которое уже в order со статусами Confirmed or Wait - их купить нельзя
            List<TicketDto> result = new List<TicketDto>();

            //все билеты на данный ивент
            List<Ticket> tickets = _ticketRepository.GetItemList().Where(x => x.EventInfoKey == eventId).ToList();

            List<Order> orders = _orderRepository.GetItemList().ToList();

            foreach (var ticket in tickets)
            {
                var order = orders.FirstOrDefault(x => x.TicketInfoKey == ticket.Id);
                if (order != null && (order.Status == StatusEnum.Selling.ToString() || order.Status == StatusEnum.Rejected.ToString()))
                {
                    result.Add(ticket.Mapper(ticket));
                }
                else
                {
                    if (order == null)
                    {
                        result.Add(ticket.Mapper(ticket));
                    }
                }
            }

            return result;
        }
    }
}
