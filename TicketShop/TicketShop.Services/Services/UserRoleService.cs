﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketShop.DataAccess;
using TicketShop.DataAccess.Enums;
using TicketShop.DataAccess.Models;
using TicketShop.Services.Extensions;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;

namespace TicketShop.Services.Services
{
    public class UserRoleService : IUserRoleService
    {
        readonly IRepository<UserRole, UserRoleKey> _userRoleRepository;

        public UserRoleService(IRepository<UserRole, UserRoleKey> userRoleRepository)
        {
            _userRoleRepository = userRoleRepository;
        }

        public List<UserRoleDto> GetUserRoles()
        {
            List<UserRole> userRoles = _userRoleRepository.GetItemList().ToList();
            var result = userRoles.Mapper(userRoles);
            
            return result;
        }


        public List<UserRoleDto> GetUserRolesByUserId(int userId)
        {
            List<UserRole> userRoles = _userRoleRepository.GetItemList().Where(userRole => userRole.UserId == userId).ToList();
            var result = userRoles.Mapper(userRoles);

            return result;
        }


        public List<UserRoleDto> GetUserRolesByRoleId(int roleId)
        {
            List<UserRole> userRoles = _userRoleRepository.GetItemList().Where(userRole => userRole.RoleId == roleId).ToList();
            var result = userRoles.Mapper(userRoles);

            return result;
        }

        public UserRoleDto GetUserRoleByUserName(string userEmail)
        {
            UserRole userRole = _userRoleRepository.GetItemList().FirstOrDefault(x => x.User.Email == userEmail);
            var result = userRole.Mapper(userRole);

            return result;
        }

        public UserRoleDto GetUserRoleByRoleName(string roleName)
        {
            UserRole userRole =
                _userRoleRepository.GetItemList()
                    .FirstOrDefault(x => Enum.GetName(typeof(UserRoleEnum), x.Role.Name) == roleName);
            var result = userRole.Mapper(userRole);

            return result;
        }

        public void Create(UserRoleDto userRoleDto)
        {
            var result = userRoleDto.Mapper(userRoleDto);

            _userRoleRepository.Create(result);
            _userRoleRepository.Save();
        }

        public void Delete(UserRoleDto userRoleDto)
        {
            UserRoleKey userRoleKey = new UserRoleKey
            {
                UserId = userRoleDto.UserId,
                RoleId = userRoleDto.RoleId
            };

            _userRoleRepository.Delete(userRoleKey);
            _userRoleRepository.Save();
        }
    }
}
