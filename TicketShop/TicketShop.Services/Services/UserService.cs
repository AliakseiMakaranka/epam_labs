﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using TicketShop.DataAccess;
using TicketShop.DataAccess.Enums;
using TicketShop.DataAccess.Models;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;

namespace TicketShop.Services.Services
{
    public class UserService : IUserService
    {
        readonly IRepository<User, int> _userRepository;
        readonly IRepository<UserRole, UserRoleKey> _userRoleRepository;

        public UserService(IRepository<User, int> userRepository, IRepository<UserRole, UserRoleKey> userRoleRepository)
        {
            _userRepository = userRepository;
            _userRoleRepository = userRoleRepository;
        }

        public List<UserDto> GetUsers()
        {
            IEnumerable<User> users = _userRepository.GetItemList();

            List<UserDto> usersDto = new List<UserDto>();

            foreach (var user in users)
            {
                //find all roles
                List<UserRoleDto> userRoles = new List<UserRoleDto>();

                foreach (var userRole in user.UserRoles)
                {
                    UserRoleDto _userRole = new UserRoleDto
                    {
                        UserId = user.Id,
                        RoleId = userRole.RoleId,
                        Role = new RoleDto
                        {
                            Id = userRole.Role.Id,
                            Name = userRole.Role.Name
                        },
                        User = new UserDto
                        {
                            Id = userRole.User.Id
                        }
                    };

                    userRoles.Add(_userRole);
                }

                UserDto userDto = new UserDto
                {
                    Id = user.Id,
                    Address = user.Address,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Localization = user.Localization,
                    Password = user.Password,
                    PhoneNumber = user.PhoneNumber,
                    UserRoles = userRoles
                };

                usersDto.Add(userDto);
            }

            return usersDto;
        }

        public UserDto GetUser(int userId)
        {
            User user = _userRepository.GetItem(userId);
            UserDto userDto = new UserDto
            {
                Id = user.Id,
                Address = user.Address,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Localization = user.Localization,
                Password = user.Password,
                PhoneNumber = user.PhoneNumber
            };

            return userDto;
        }

        public UserDto GetUser(string email)
        {
            User user = _userRepository.GetItemList().FirstOrDefault(x => x.Email == email);

            UserDto result;

            if (user != null)
            {
                List<UserRoleDto> userRolesDto = new List<UserRoleDto>();

                foreach (var userUserRole in user.UserRoles)
                {
                    UserRoleDto userRoleDto = new UserRoleDto
                    {
                        RoleId = userUserRole.RoleId,
                        UserId = userUserRole.UserId,
                        User = new UserDto
                        {
                            Id = userUserRole.User.Id
                        },
                        Role = new RoleDto
                        {
                            Id = userUserRole.Role.Id,
                            Name = userUserRole.Role.Name
                        }
                    };

                    userRolesDto.Add(userRoleDto);
                }

                result = new UserDto
                {
                    Id = user.Id,
                    Address = user.Address,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Localization = user.Localization,
                    Password = user.Password,
                    PhoneNumber = user.PhoneNumber,
                    UserRoles = userRolesDto
                };
            }
            else
            {
                return null; //нужно при регистрации, мол такого чела не найдено - значит создаем
            }

            return result;
        }

        public void Create(UserDto userDto)
        {
            List<UserRole> userRoles = new List<UserRole>();

            foreach (var role in userDto.UserRoles)
            {
                var userRole = new UserRole
                {
                    UserId = role.UserId,
                    RoleId = role.RoleId
                };

                userRoles.Add(userRole);
            }

            User user = new User
            {
                Id = userDto.Id,
                FirstName = userDto.FirstName,
                LastName = userDto.LastName,
                Address = userDto.Address,
                Localization = userDto.Localization,
                Password = userDto.Password,
                Email = userDto.Email,
                PhoneNumber = userDto.PhoneNumber,
                UserRoles = userRoles
            };

            _userRepository.Create(user);
            //кроме юзера так же нужно создать связи
            foreach (var userUserRole in user.UserRoles)
            {
                _userRoleRepository.Create(userUserRole);
            }

            _userRepository.Save();
        }

        //:todo пересмотреть логику
        public SelectList GetLanguageList()
        {
            var local = Enum.GetValues(typeof(LocalizationEnum))
                .Cast<LocalizationEnum>()
                .Select(x => new { Id = x, Value = x })
                .ToList();

            return new SelectList(local, "Id", "Value");
        }

        public void Edit(UserDto userDto)
        {
            throw new NotImplementedException();
        }

        public void Delete(UserDto userDto)
        {
            _userRepository.Delete(userDto.Id);
            _userRepository.Save();
        }
    }
}