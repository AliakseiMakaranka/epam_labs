﻿using System.Collections.Generic;
using System.Linq;
using TicketShop.DataAccess;
using TicketShop.DataAccess.Models;
using TicketShop.Services.Extensions;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;

namespace TicketShop.Services.Services
{
    public class VenueService : IVenueService
    {
        readonly IRepository<Venue, int> _venuesRepository;

        public VenueService(IRepository<Venue, int> venueRepository)
        {
            _venuesRepository = venueRepository;
        }

        public List<VenueDto> GetVenues()
        {
            List<Venue> venues = _venuesRepository.GetItemList().ToList();
            var result = venues.Mapper(venues);
            
            return result;
        }

        public VenueDto GetVenue(int venueId)
        {
            Venue venue = _venuesRepository.GetItem(venueId);
            var result = venue.Mapper(venue);
            
            return result;
        }

        public VenueDto GetVenue(string venueName)
        {
            Venue venue = _venuesRepository.GetItemList().FirstOrDefault(x => x.Name == venueName);
            var result = venue.Mapper(venue);
            
            return result;
        }

        public void Create(VenueDto venueDto)
        {
            var result = venueDto.Mapper(venueDto);
            
            _venuesRepository.Create(result);
            _venuesRepository.Save();
        }

        public void Edit(VenueDto venueDto)
        {
            var result = venueDto.Mapper(venueDto);
            
            _venuesRepository.Update(result);
            _venuesRepository.Save();
        }

        public void Delete(VenueDto venueDto)
        {
            _venuesRepository.Delete(venueDto.Id);
            _venuesRepository.Save();
        }
    }
}
