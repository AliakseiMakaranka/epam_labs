﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace TicketShop.Web
{
    public class AuthorizeActionFilter : IAsyncActionFilter
    {
        private readonly string _item;

        public AuthorizeActionFilter(string item)
        {
            _item = item;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            bool isAuthorize = false;
            var roles = _item.Replace(" ", "").Split(',');

            var userRole = context.HttpContext.User.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Role).Value;

            foreach (var role in roles)
            {
                if (userRole.Contains(role))
                {
                    isAuthorize = true;
                    await next();
                    break;
                }
            }

            if (isAuthorize == false)
                context.Result = new UnauthorizedResult();
        }
    }
}
