﻿using Microsoft.AspNetCore.Mvc;

namespace TicketShop.Web
{
    public class AuthorizeAttribute : TypeFilterAttribute
    {
        public AuthorizeAttribute(string item)
            : base(typeof(AuthorizeActionFilter))
        {
            Arguments = new object[] { item };
        }
    }
}
