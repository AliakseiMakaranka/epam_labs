﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TicketShop.DataAccess.Enums;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;
using TicketShop.Web.Extensions;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Controllers
{
    public class AccountController : AuthenticateController
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginWeb model)
        {
            if (ModelState.IsValid)
            {
                UserDto foundedUserDto = _userService.GetUser(model.Email);

                if (foundedUserDto == null || foundedUserDto.Password != model.Password)
                {
                    ModelState.AddModelError("Email", "Incorrect login and/or password");
                }
                else
                {
                    var loginWeb = foundedUserDto.UserDtoToLoginWeb(foundedUserDto);

                    if (loginWeb != null)
                    {
                        await Authenticate(loginWeb); // аутентификация

                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            RegisterWeb model = new RegisterWeb();
            model.LanguageList = _userService.GetLanguageList();

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterWeb model)
        {
            if (ModelState.IsValid)
            {
                UserDto foundedUserDto = _userService.GetUser(model.Email);

                if (foundedUserDto == null)
                {
                    UserDto userDto = model.RegisterWebToUserDto(model, UserRoleEnum.User);
                    _userService.Create(userDto);

                    LoginWeb loginWeb = userDto.UserDtoToLoginWeb(userDto);
                    await Authenticate(loginWeb);

                    return RedirectToAction("Index", "Home");
                }
                if (model.Password != model.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Incorrect login and/or password");
                }
                return RedirectToAction("Register", "Account");
            }

            RegisterWeb result = new RegisterWeb
            {
                LanguageList = _userService.GetLanguageList()
            };

            return View(result);
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize("Admin, User")]
        public IActionResult UserInfo()
        {
            UserDto foundedUserDto = _userService.GetUser(User.Identity.Name);
            var result = foundedUserDto.UserDtoToUserWeb(foundedUserDto);

            return View(result);
        }
    }
}