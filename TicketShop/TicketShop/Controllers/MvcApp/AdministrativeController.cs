﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TicketShop.DataAccess.Enums;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;
using TicketShop.Web.Extensions;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Controllers
{
    public class AdministrativeController : AuthenticateController
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ICityService _cityService;
        private readonly IVenueService _venueService;
        private readonly IEventService _eventService;
        private readonly IUserService _userService;
        private readonly IUserRoleService _userRolesService;

        public AdministrativeController(
            ICityService cityService,
            IVenueService venueService,
            IEventService eventService,
            IUserService userService,
            IUserRoleService userRolesService,
            IHostingEnvironment hostingEnvironment)
        {
            _cityService = cityService;
            _venueService = venueService;
            _eventService = eventService;
            _userService = userService;
            _userRolesService = userRolesService;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        [Authorize("Admin")]
        public IActionResult Adminka()
        {
            return View();
        }

        #region City

        [HttpGet]
        [Authorize("Admin")]
        public IActionResult Cities()
        {
            List<CityDto> foundedCities = _cityService.GetCities().ToList();
            List<CityWeb> citiesWeb = foundedCities.CityDtoToCityWeb(foundedCities);

            return View(citiesWeb);
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> AddCity()
        {
            return View();
        }

        [HttpPost]
        [Authorize("Admin")]
        public async Task<IActionResult> AddCity(CityWeb cityWebInput)
        {
            if (ModelState.IsValid)
            {
                if (cityWebInput != null)
                {
                    CityDto cityDto = cityWebInput.CityWebToCityDto(cityWebInput);
                    _cityService.Create(cityDto);

                    return RedirectToAction("Cities", "Administrative");
                }
            }

            return View();
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> EditCity(int cityId)
        {
            CityDto cityDto = _cityService.GetCity(cityId);
            CityWeb cityWeb = cityDto.CityDtoToCityWeb(cityDto);

            return View(cityWeb);
        }

        [HttpPost]
        [Authorize("Admin")]
        public async Task<IActionResult> EditCity(CityWeb city)
        {
            if (ModelState.IsValid)
            {
                if (city != null)
                {
                    CityDto cituDto = city.CityWebToCityDto(city);
                    _cityService.Edit(cituDto);

                    return RedirectToAction("Cities", "Administrative");
                }
            }

            return View(city);
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> RemoveCity(int cityId)
        {
            _cityService.Delete(new CityDto { Id = cityId });

            return RedirectToAction("Cities", "Administrative");
        }

        #endregion

        #region Venue

        [HttpGet]
        [Authorize("Admin")]
        public IActionResult Venues()
        {
            List<VenueDto> foundedVenues = _venueService.GetVenues().ToList();
            List<VenueWeb> venuesWeb = foundedVenues.VenueDtoToVenueWeb(foundedVenues);

            return View(venuesWeb);
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> AddVenue()
        {
            List<CityDto> citiesDto = _cityService.GetCities().ToList();

            ExtentionVenueWeb extentionVenueWeb = new ExtentionVenueWeb
            {
                AllCities = citiesDto.CityDtoToCityWeb(citiesDto)
            };

            return View(extentionVenueWeb);
        }

        [HttpPost]
        [Authorize("Admin")]
        public async Task<IActionResult> AddVenue(ExtentionVenueWeb extentionVenueWebInput)
        {
            if (ModelState.IsValid)
            {
                if (extentionVenueWebInput != null)
                {
                    CityDto city = _cityService.GetCity(extentionVenueWebInput.CityNameInput);
                    extentionVenueWebInput.CityInfoKey = city.Id;
                    VenueDto venuesDto = extentionVenueWebInput.ExtentionVenueWebToVenueDto(extentionVenueWebInput);
                    _venueService.Create(venuesDto);

                    return RedirectToAction("Venues", "Administrative");
                }
            }
            List<CityDto> citiesDto = _cityService.GetCities().ToList();
            extentionVenueWebInput.AllCities = citiesDto.CityDtoToCityWeb(citiesDto);
            
            return View(extentionVenueWebInput);
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> EditVenue(int venueId)
        {
            VenueDto venueDto = _venueService.GetVenue(venueId);
            //маппим
            ExtentionVenueWeb extentionVenuesWeb = venueDto.VenueDtoToExtentionVenueWeb(venueDto);
            //нужно добавить в модель все города, чтобы потом был выбор для edit
            List<CityDto> citiesDto = _cityService.GetCities().ToList();
            //мапим и добавляем в модель
            extentionVenuesWeb.AllCities = citiesDto.CityDtoToCityWeb(citiesDto);
            //прописываем поле CityNameInput для подсказки на форме
            string cityName = _cityService.GetCity(venueDto.CityInfoKey).Name;
            extentionVenuesWeb.CityNameInput = cityName;

            return View(extentionVenuesWeb);
        }

        [HttpPost]
        [Authorize("Admin")]
        public async Task<IActionResult> EditVenue(ExtentionVenueWeb extentionVenueWebInput)
        {
            if (ModelState.IsValid)
            {

                if (extentionVenueWebInput != null)
                {
                    extentionVenueWebInput.CityInfoKey = _cityService.GetCity(extentionVenueWebInput.CityNameInput).Id;
                    VenueDto venueDto = extentionVenueWebInput.ExtentionVenueWebToVenueDto(extentionVenueWebInput);
                    _venueService.Edit(venueDto);

                    return RedirectToAction("Venues", "Administrative");
                }
            }

            List<CityDto> citiesDto = _cityService.GetCities().ToList();
            extentionVenueWebInput.AllCities = citiesDto.CityDtoToCityWeb(citiesDto);

            return View(extentionVenueWebInput);
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> RemoveVenue(int venueId)
        {
            _venueService.Delete(new VenueDto { Id = venueId });
            return RedirectToAction("Venues", "Administrative");
        }

        #endregion

        #region Event

        [HttpGet]
        [Authorize("Admin")]
        public IActionResult Events()
        {
            List<EventDto> foundedEvents = _eventService.GetEvents().ToList();
            List<EventWeb> eventsWeb = foundedEvents.EventDtoToEventWeb(foundedEvents);

            return View(eventsWeb);
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> AddEvent()
        {
            List<VenueDto> venuesDto = _venueService.GetVenues().ToList();
            List<ExtentionVenueWeb> extentionVenueWebs = venuesDto.VenueDtoToExtentionVenueWeb(venuesDto);
            ExtentionEventWeb extentionEventWeb = new ExtentionEventWeb
            {
                AllVenues = extentionVenueWebs
            };

            return View(extentionEventWeb);
        }

        [HttpPost]
        [Authorize("Admin")]
        public async Task<IActionResult> AddEvent(
            ExtentionEventWeb extentionEventWebInput,
            IFormFile file)
        {
            if (file != null)
            {
                string webRootPath = _hostingEnvironment.WebRootPath;
                if (file.Length > 0)
                {
                    using (var stream = new FileStream(webRootPath + "/Images/" + file.FileName, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                if (extentionEventWebInput != null)
                {
                    string banner = String.Empty;

                    if (file != null)
                    {
                        banner = "~/Images/" + file.FileName;
                    }

                    extentionEventWebInput.Banner = banner;
                    extentionEventWebInput.VenueInfoKey = _venueService.GetVenue(extentionEventWebInput.VenueName).Id;
                    EventDto eventDto = extentionEventWebInput.EventDtoToEventWeb(extentionEventWebInput);
                    _eventService.Create(eventDto);

                    return RedirectToAction("Events", "Administrative");
                }
            }

            List<VenueDto> venuesDto = _venueService.GetVenues().ToList();
            List<ExtentionVenueWeb> extentionVenueWebs = venuesDto.VenueDtoToExtentionVenueWeb(venuesDto);
            extentionEventWebInput.AllVenues = extentionVenueWebs;
            
            return View(extentionEventWebInput);
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> EditEvent(int eventId)
        {
            EventDto eventDto = _eventService.GetEvent(eventId);
            //находим название venue для подсказки
            var venueName = _venueService.GetVenue(eventDto.VenueInfoKey).Name;
            //маппим
            ExtentionEventWeb extentionEventsWeb = eventDto.EventDtoToExtentionEventWeb(eventDto);
            List<VenueDto> venuesDto = _venueService.GetVenues().ToList();
            List<ExtentionVenueWeb> extentionVenueWebs = venuesDto.VenueDtoToExtentionVenueWeb(venuesDto);
            extentionEventsWeb.AllVenues = extentionVenueWebs;
            //прописываем поле VenueName для подсказки на форме
            extentionEventsWeb.VenueName = venueName;
            extentionEventsWeb.Id = eventId;

            return View(extentionEventsWeb);
        }

        [HttpPost]
        [Authorize("Admin")]
        public async Task<IActionResult> EditEvent(ExtentionEventWeb extentionEventWebInput, IFormFile file)
        {
            string banner = string.Empty;

            if (file != null)
            {
                banner = "~/Images/" + file.FileName;
                string webRootPath = _hostingEnvironment.WebRootPath;

                if (file.Length > 0)
                {
                    using (var stream = new FileStream(webRootPath + "/Images/" + file.FileName, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                if (extentionEventWebInput != null)
                {
                    extentionEventWebInput.VenueInfoKey = _venueService.GetVenue(extentionEventWebInput.VenueName).Id;
                    if (banner != String.Empty)
                    {
                        extentionEventWebInput.Banner = banner;
                    }

                    EventDto eventDto = extentionEventWebInput.ExtentionEventWebToEventDto(extentionEventWebInput);
                    _eventService.Edit(eventDto);

                    return RedirectToAction("Events", "Administrative");
                }
            }
            
            ExtentionEventWeb extentionEventWebInput2 = new ExtentionEventWeb();
            List<VenueDto> venuesDtos = _venueService.GetVenues().ToList();
            List<ExtentionVenueWeb> extentionVenueWebss = venuesDtos.VenueDtoToExtentionVenueWeb(venuesDtos);
            extentionEventWebInput2.AllVenues = extentionVenueWebss;

            return View(extentionEventWebInput2);
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> RemoveEvent(int eventId)
        {
            _eventService.Delete(new EventDto { Id = eventId });

            return RedirectToAction("Events", "Administrative");
        }

        #endregion

        #region UserControl
        [HttpGet]
        [Authorize("Admin")]
        public IActionResult RegisterAdmin()
        {
            RegisterWeb model = new RegisterWeb();
            model.LanguageList = _userService.GetLanguageList();

            return View(model);
        }

        [HttpPost]
        [Authorize("Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegisterAdmin(RegisterWeb model)
        {
            if (ModelState.IsValid)
            {
                UserDto foundedUserDto = _userService.GetUser(model.Email);

                if (foundedUserDto == null)
                {
                    UserDto userDto = model.RegisterWebToUserDto(model, UserRoleEnum.Admin);
                    _userService.Create(userDto);

                    LoginWeb loginWeb = userDto.UserDtoToLoginWeb(userDto);
                    await Authenticate(loginWeb);

                    return RedirectToAction("Index", "Home");
                }
                if (model.Password != model.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Incorrect login and/or password");
                }
                return RedirectToAction("RegisterAdmin", "Administrative");
            }

            RegisterWeb result = new RegisterWeb
            {
                LanguageList = _userService.GetLanguageList()
            };

            return View(result);
        }

        [HttpGet]
        [Authorize("Admin")]
        public IActionResult UserControlMenu()
        {
            List<UserDto> allUsersDto = _userService.GetUsers().ToList();

            List<UserWeb> usersWeb = allUsersDto.UserDtoToUserWeb(allUsersDto);

            return View(usersWeb);
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> RemoveUser(int userId)
        {
            _userService.Delete(new UserDto { Id = userId });

            return RedirectToAction("UserControlMenu", "Administrative");
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> AddAdminRole(int userId)
        {
            List<UserRoleDto> userRolesDto = _userRolesService.GetUserRolesByUserId(userId);

            if (userRolesDto.Any(x => x.RoleId == (int)UserRoleEnum.Admin))
            {
                _userRolesService.Delete(userRolesDto.First(x => x.RoleId == (int)UserRoleEnum.Admin));
            }
            else
            {
                UserRoleDto userRoleDto = new UserRoleDto
                {
                    UserId = userId,
                    RoleId = (int)UserRoleEnum.Admin
                };

                _userRolesService.Create(userRoleDto);
            }

            return RedirectToAction("UserControlMenu", "Administrative");
        }

        [HttpGet]
        [Authorize("Admin")]
        public async Task<IActionResult> AddUserRole(int userId)
        {
            List<UserRoleDto> userRolesDto = _userRolesService.GetUserRolesByUserId(userId);

            if (userRolesDto.Any(x => x.RoleId == (int)UserRoleEnum.User))
            {
                _userRolesService.Delete(userRolesDto.First(x => x.RoleId == (int)UserRoleEnum.User));
            }
            else
            {
                UserRoleDto userRoleDto = new UserRoleDto
                {
                    UserId = userId,
                    RoleId = (int)UserRoleEnum.User
                };

                _userRolesService.Create(userRoleDto);
            }

            return RedirectToAction("UserControlMenu", "Administrative");
        }
        #endregion
    }
}
