﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Controllers
{
    public abstract class AuthenticateController : Controller
    {
        public List<Claim> Claims;

        public async Task Authenticate(LoginWeb user)
        {
            string roles = string.Empty;

            foreach (var role in user.UserRoles)
            {
                roles += role.Role.Name;
            }

            Claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Email),
                new Claim(ClaimTypes.Role,roles),
                new Claim(ClaimTypes.Locality, user.Localization)
            };
            ClaimsIdentity id = new ClaimsIdentity(Claims, "ApplicationCookie",
                ClaimTypes.Name, ClaimTypes.Role
            );

            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }
    }
}
