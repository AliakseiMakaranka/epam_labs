﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;
using TicketShop.Web.Extensions;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Controllers.MvcApp
{
    public class EventController : Controller
    {
        private readonly IEventService _eventService;

        public EventController(IEventService eventService)
        {
            _eventService = eventService;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Events()
        {
            List<EventDto> foundedEventsDto = _eventService.GetActualDateEvents().ToList();
            List<EventWeb> eventsWeb = foundedEventsDto.EventDtoToEventWeb(foundedEventsDto);

            return View(eventsWeb);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ExtendedFindEvents()
        {
            List<EventDto> foundedEventsDto = _eventService.GetActualDateEvents().ToList();
            List<EventWeb> eventsWeb = foundedEventsDto.EventDtoToEventWeb(foundedEventsDto);

            return View(eventsWeb);
        }
    }
}
