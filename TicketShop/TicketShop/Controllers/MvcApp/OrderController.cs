﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;
using TicketShop.Web.Extensions;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrdersService _ordersService;
        private readonly IUserService _userService;
        private readonly ITicketService _ticketService;

        public OrderController(IOrdersService ordersService, IUserService userService, ITicketService ticketService)
        {
            _ordersService = ordersService;
            _userService = userService;
            _ticketService = ticketService;
        }

        [HttpGet]
        [Authorize("Admin, User")]
        public IActionResult AddToOrder(int ticketId)
        {
            int buyerId =
                _userService.GetUsers().FirstOrDefault(name => name.Email == HttpContext.User.Identity.Name).Id;
            _ordersService.AddToOrder(buyerId, ticketId);

            return View();
        }

        [HttpGet]
        [Authorize("Admin, User")]
        public IActionResult MyOrders()
        {
            List<TicketDto> tickets = _ticketService.GetTickets().ToList();

            List<OrderDto> foundedOrdersDto = _ordersService.GetOrdersByUserName(User.Identity.Name).ToList();
            List<MyOrdersWeb> myOrdersWebs = foundedOrdersDto.OrderDtoToMyOrderWeb(foundedOrdersDto, tickets);

            return View(myOrdersWebs);
        }
    }
}
