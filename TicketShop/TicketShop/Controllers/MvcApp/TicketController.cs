﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;
using TicketShop.Web.Extensions;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Controllers
{
    public class TicketController : Controller
    {
        private readonly ITicketService _ticketService;
        private readonly IEventService _eventService;
        private readonly IUserService _userService;
        private readonly IOrdersService _orderService;

        public TicketController(ITicketService ticketService, IEventService eventService, IUserService userService,
            IOrdersService orderService)
        {
            _ticketService = ticketService;
            _eventService = eventService;
            _userService = userService;
            _orderService = orderService;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Tickets(int eventId)
        {
            var freeTickets = _ticketService.GetSellingTicketsOfEvent(eventId).ToList();
            var result = freeTickets.TicketsDtoToTicketsWeb(freeTickets);

            return View(result);
        }

        [HttpGet]
        [Authorize("Admin, User")]
        public IActionResult CreateTicket()
        {
            List<EventDto> eventsDto = _eventService.GetActualDateEvents().ToList();
            List<EventWeb> eventsWeb = new List<EventWeb>();

            foreach (var eventDto in eventsDto)
            {
                EventWeb eventWeb = new EventWeb
                {
                    Name = eventDto.Name
                };

                eventsWeb.Add(eventWeb);
            }

            CreateTicketWeb createTicketWeb = new CreateTicketWeb
            {
                EventsWeb = eventsWeb
            };

            return View(createTicketWeb);
        }

        [HttpPost]
        [Authorize("Admin, User")]
        public IActionResult CreateTicket(CreateTicketWeb ticketWeb)
        {
            if (ModelState.IsValid)
            {
                if (ticketWeb != null)
                {
                    TicketDto ticketDto = new TicketDto
                    {
                        Price = ticketWeb.Price,
                        EventInfoKey = _eventService.GetEvent(ticketWeb.EventName).Id,
                        SellerInfoKey = _userService.GetUser(User.Identity.Name).Id
                    };

                    _ticketService.Create(ticketDto);

                    return RedirectToAction("Index", "Home");
                }
            }

            return CreateTicket();
        }

        [HttpGet]
        [Authorize("Admin, User")]
        public IActionResult SuccesfulCreatedTicket()
        {
            return View();
        }

        [HttpGet]
        [Authorize("Admin, User")]
        public IActionResult ConfirmationTrackNo(int ticketId)
        {
            return View();
        }

        [HttpGet]
        [Authorize("Admin, User")]
        public IActionResult RejectionTrackNo(int ticketId)
        {
            return View();
        }

        [HttpPost]
        [Authorize("Admin, User")]
        public IActionResult Confirmation(ConfirmationTicketBuy confirm)
        {
            //меняем статус ордера с этим тикетом на confirm, присваиваем трэк номер и кидаем его покупателю
            _orderService.Confirm(confirm.TicketId, confirm.TrackNo);

            return RedirectToAction("MyTicketsWait", "Ticket");
        }

        [HttpPost]
        [Authorize("Admin, User")]
        public IActionResult Rejection(ConfirmationTicketBuy confirm)
        {
            //меняем статус тикета на reject, пишем коментарий (по нажатию на крестик всплывает окно (или новое для начала) с вводом коментария, потом выводим его у пользователя
            _orderService.Reject(confirm.TicketId, confirm.Comment);
            return RedirectToAction("MyTicketsWait", "Ticket");
        }

        [HttpGet]
        [Authorize("Admin, User")]
        public IActionResult MyTicketsSelling()
        {
            List<TicketDto> foundedTicketsDto =
                _ticketService.GetTicketsByUserName(User.Identity.Name)
                    .Where(x => x.Event.Date >= DateTime.Now)
                    .ToList();
            List<OrderDto> orders = _orderService.GetOrders().ToList();
            List<MyTicketsWeb> myTicketsWeb = foundedTicketsDto.TicketsDtoToMyTicketsWeb(foundedTicketsDto, orders);

            return View(myTicketsWeb);
        }

        [HttpGet]
        [Authorize("Admin, User")]
        public IActionResult MyTicketsWait()
        {
            List<TicketDto> foundedTicketsDto = _ticketService.GetTicketsByUserName(User.Identity.Name).ToList();
            List<OrderDto> orders = _orderService.GetOrders().ToList();
            List<MyTicketsWeb> myTicketsWeb = foundedTicketsDto.TicketsDtoToMyTicketsWeb(foundedTicketsDto, orders);

            return View(myTicketsWeb);
        }

        [HttpGet]
        [Authorize("Admin, User")]
        public IActionResult MyTicketsSold()
        {
            List<TicketDto> foundedTicketsDto = _ticketService.GetTicketsByUserName(User.Identity.Name).ToList();
            List<OrderDto> orders = _orderService.GetOrders().ToList();
            List<MyTicketsWeb> myTicketsWeb = foundedTicketsDto.TicketsDtoToMyTicketsWeb(foundedTicketsDto, orders);

            return View(myTicketsWeb);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ErrorWhenBuyWithoutAuthorization()
        {
            return View();
        }
    }
}
