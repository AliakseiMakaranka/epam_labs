﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;
using TicketShop.Web.Extensions;
using TicketShop.Web.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketShop.Web.Controllers.WebApi
{
    [Route("api/[controller]")]
    public class CitiesController : Controller
    {
        private readonly ICityService _cityService;

        public CitiesController(ICityService cityService)
        {
            _cityService = cityService;
        }

        // GET: api/values
        [HttpGet]
        [FormatFilter]
        public IActionResult Get(string[] citiesNames)
        {
            List<CityDto> citiesDtos = _cityService.GetCities();

            List<CityWeb> result= new List<CityWeb>();

            if (citiesNames != null)
            {
                foreach (var citiesName in citiesNames)
                {
                    var citiesDto = citiesDtos.Where(x => x.Name.Contains(citiesName)).ToList();
                    var citiesWeb = citiesDto.CityDtoToCityWeb(citiesDto);

                    var newItems = citiesWeb.Where(x => result.All(y => x.Id != y.Id));

                    foreach (var item in newItems)
                    {
                        result.Add(item);
                    }
                }
            }

            return new ObjectResult(result);
        }
    }
}
