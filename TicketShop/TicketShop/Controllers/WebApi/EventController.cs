﻿//using System;
//using System.Collections.Generic;
//using System.Net.Http;
//using Microsoft.AspNetCore.Mvc;
//using TicketShop.Services.Services.Interfaces;
//using TicketShop.Web.Extensions;
//using TicketShop.Web.ViewModels;

//namespace TicketShop.Web.Controllers.WebApi
//{
//    [Route("api/[controller]")]
//    public class EventController : Controller
//    {
//        private readonly IEventService _eventService;

//        public EventController(IEventService eventService)
//        {
//            _eventService = eventService;
//        }
        
//    // GET: api/values
//        [HttpGet]
//        [FormatFilter]
//        public IActionResult Get(
//            string eventName,
//            string beginDate,
//            string endDate,
//            int[] venuesIds,
//            int[] citiesIds,
//            int pageSize,
//            int pageNumber = 1)
//        {            
//            var eventDto = _eventService.GetEventOverlap(eventName, beginDate, endDate, venuesIds, citiesIds, pageSize, pageNumber);
//            var result = eventDto.EventDtoToEventWeb(eventDto);

//            return new ObjectResult(result);
//        }        
//    }
//}
