﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TicketShop.Services.Services.Interfaces;
using TicketShop.Web.Extensions;
using TicketShop.Web.ViewModelsPages;

namespace TicketShop.Web.Controllers.WebApi
{
    [Route("api/[controller]")]
    public class EventPagesController : Controller
    {
        private readonly IEventService _eventService;

        public EventPagesController(IEventService eventService)
        {
            _eventService = eventService;
        }
        
    // GET: api/values
        [HttpGet]
        [FormatFilter]
        public IActionResult Get(
            string eventName,
            string beginDate,
            string endDate,
            int[] venuesIds,
            int[] citiesIds,
            int pageSize,
            int pageNumber = 1)
        {            
            var eventDto = _eventService.GetEventOverlap(eventName, beginDate, endDate, venuesIds, citiesIds,
                pageSize, pageNumber);

            var eventWebs = eventDto.EventDtoToEventWeb(eventDto);
            
            //берем нужное кол-во записей
            var temp = eventWebs;
            if (pageSize == 0)
            {
                eventWebs = temp.Take(temp.Count).ToList();
            }
            else
            {
                eventWebs = temp.Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList();
            }

            var result = new EventWebPages
            {
                EventWebs = eventWebs,
                PageInfo = new PageInfo
                {
                    PageSize = pageSize,
                    PageNumber = pageNumber,
                    TotalItems = temp.Count
                }
            };

            return new ObjectResult(result);
        }        
    }
}
