﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TicketShop.Services.ModelsDto;
using TicketShop.Services.Services.Interfaces;
using TicketShop.Web.Extensions;
using TicketShop.Web.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketShop.Web.Controllers.WebApi
{
    [Route("api/[controller]")]
    public class VenuesController : Controller
    {

        private readonly IVenueService _venueService;

        public VenuesController(IVenueService venueService)
        {
            _venueService = venueService;
        }

        // GET: api/values
        [HttpGet]
        [FormatFilter]
        public IActionResult Get(string[] venuesNames)
        {
            List<VenueDto> citiesDtos = _venueService.GetVenues();

            List<VenueWeb> result = new List<VenueWeb>();

            if (venuesNames != null)
            {
                foreach (var venuesName in venuesNames)
                {
                    var venuesDto = citiesDtos.Where(x => x.Name.Contains(venuesName)).ToList();
                    var venuesWeb = venuesDto.VenueDtoToVenueWeb(venuesDto);

                    var newItems = venuesWeb.Where(x => result.All(y => x.Id != y.Id));

                    foreach (var item in newItems)
                    {
                        result.Add(item);
                    }
                }
            }

            return new ObjectResult(result);
        }
    }
}
