﻿using System.Collections.Generic;
using TicketShop.Services.ModelsDto;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Extensions
{
    public static class City
    {
        //lists
        public static List<CityWeb> CityDtoToCityWeb(this List<CityDto> cityDtoType, List<CityDto> foundedcitiesDto)
        {
            List<CityWeb> citiessWeb = new List<CityWeb>();

            foreach (var cityDto in foundedcitiesDto)
            {
                var cityWeb = new CityWeb
                {
                    Id = cityDto.Id,
                    Name = cityDto.Name
                };

                citiessWeb.Add(cityWeb);
            }

            return citiessWeb;
        }

        public static List<CityDto> CityWebToCityDto(this List<CityWeb> cityWebType, List<CityWeb> foundedcitiesWeb)
        {
            List<CityDto> citiessDto = new List<CityDto>();

            foreach (var cityWeb in foundedcitiesWeb)
            {
                var cityDto = new CityDto
                {
                    Id = cityWeb.Id,
                    Name = cityWeb.Name
                };

                citiessDto.Add(cityDto);
            }

            return citiessDto;
        }

        //Repeat for one
        public static CityWeb CityDtoToCityWeb(this CityDto cityDtoType, CityDto foundedcitiesDto)
        {
            var cityWeb = new CityWeb
            {
                Id = foundedcitiesDto.Id,
                Name = foundedcitiesDto.Name
            };

            return cityWeb;
        }

        public static CityDto CityWebToCityDto(this CityWeb cityWebType, CityWeb foundedcitiesWeb)
        {

            var cityDto = new CityDto
            {
                Id = foundedcitiesWeb.Id,
                Name = foundedcitiesWeb.Name
            };

            return cityDto;
        }
    }
}
