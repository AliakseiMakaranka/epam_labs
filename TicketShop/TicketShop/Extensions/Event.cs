﻿using System.Collections.Generic;
using TicketShop.Services.ModelsDto;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Extensions
{
    public static class Event
    {
        //lists
        public static List<EventWeb> EventDtoToEventWeb(this List<EventDto> eventDtoType,
            List<EventDto> foundedEventsDto)
        {
            List<EventWeb> eventsWeb = new List<EventWeb>();

            foreach (var eventDto in foundedEventsDto)
            {
                var eventWeb = new EventWeb
                {
                    Id = eventDto.Id,
                    Banner = eventDto.Banner,
                    Date = eventDto.Date,
                    Name = eventDto.Name,
                    CityName = eventDto.Venue.City.Name,
                    VenueName = eventDto.Venue.Name,
                    CityId = eventDto.Venue.City.Id
                };

                eventsWeb.Add(eventWeb);
            }

            return eventsWeb;
        }

        public static List<EventWeb> EventDtoToEventWebPages(this List<EventDto> eventDtoType,
            List<EventDto> foundedEventsDto)
        {
            List<EventWeb> eventsWeb = new List<EventWeb>();

            foreach (var eventDto in foundedEventsDto)
            {
                var eventWeb = new EventWeb
                {
                    Id = eventDto.Id,
                    Banner = eventDto.Banner,
                    Date = eventDto.Date,
                    Name = eventDto.Name,
                    CityName = eventDto.Venue.City.Name,
                    VenueName = eventDto.Venue.Name,
                    CityId = eventDto.Venue.City.Id
                };

                eventsWeb.Add(eventWeb);
            }

            return eventsWeb;
        }

        public static List<EventDto> EventDtoToEventWeb(
            this List<ExtentionEventWeb> extentionEventWebType,
            List<ExtentionEventWeb> extentionEventsWeb)
        {
            List<EventDto> eventsDto = new List<EventDto>();

            foreach (var extentionEventWeb in extentionEventsWeb)
            {
                var eventWeb = new EventDto
                {
                    Banner = extentionEventWeb.Banner,
                    Date = extentionEventWeb.Date,
                    Name = extentionEventWeb.Name,
                    VenueInfoKey = extentionEventWeb.VenueInfoKey,
                    Description = extentionEventWeb.Description
                };

                eventsDto.Add(eventWeb);
            }

            return eventsDto;
        }

        public static List<ExtentionEventWeb> EventDtoToExtentionEventWeb(
            this List<EventDto> eventDtoType,
            List<EventDto> foundedEventsDto)
        {
            List<ExtentionEventWeb> extentionEventsWeb = new List<ExtentionEventWeb>();

            foreach (var eventDto in foundedEventsDto)
            {
                var extentionEventWeb = new ExtentionEventWeb
                {
                    Banner = eventDto.Banner,
                    Date = eventDto.Date,
                    Name = eventDto.Name,
                    Description = eventDto.Description
                };

                extentionEventsWeb.Add(extentionEventWeb);
            }

            return extentionEventsWeb;
        }

        public static List<EventDto> ExtentionEventWebToEventDto(
            this List<ExtentionEventWeb> extentionEventWebType,
            List<ExtentionEventWeb> foundedExtentionEventsWeb)
        {
            List<EventDto> eventsDtoWeb = new List<EventDto>();

            foreach (var extentionEventWeb in foundedExtentionEventsWeb)
            {
                var eventDto = new EventDto
                {
                    Id = extentionEventWeb.Id,
                    Banner = extentionEventWeb.Banner,
                    Date = extentionEventWeb.Date,
                    Name = extentionEventWeb.Name,
                    Description = extentionEventWeb.Description,
                    VenueInfoKey = extentionEventWeb.VenueInfoKey
                };

                eventsDtoWeb.Add(eventDto);
            }

            return eventsDtoWeb;
        }

        //Repeat for one
        public static EventWeb EventDtoToEventWeb(this EventDto eventDtoType, EventDto foundedEventsDto)
        {
            var eventWeb = new EventWeb
            {
                Id = foundedEventsDto.Id,
                Banner = foundedEventsDto.Banner,
                Date = foundedEventsDto.Date,
                Name = foundedEventsDto.Name,
                CityName = foundedEventsDto.Venue.City.Name,
                VenueName = foundedEventsDto.Venue.Name
            };

            return eventWeb;
        }

        public static EventDto EventDtoToEventWeb(this ExtentionEventWeb extentionEventWebType,
            ExtentionEventWeb extentionEventsWeb)
        {
            var eventDto = new EventDto
            {
                Banner = extentionEventsWeb.Banner,
                Date = extentionEventsWeb.Date,
                Name = extentionEventsWeb.Name,
                VenueInfoKey = extentionEventsWeb.VenueInfoKey,
                Description = extentionEventsWeb.Description
            };

            return eventDto;
        }

        public static ExtentionEventWeb EventDtoToExtentionEventWeb(this EventDto eventDtoType,
            EventDto foundedEventsDto)
        {
            var extentionEventWeb = new ExtentionEventWeb
            {
                Banner = foundedEventsDto.Banner,
                Date = foundedEventsDto.Date,
                Name = foundedEventsDto.Name,
                Description = foundedEventsDto.Description
            };

            return extentionEventWeb;
        }

        public static EventDto ExtentionEventWebToEventDto(
            this ExtentionEventWeb extentionEventWebType,
            ExtentionEventWeb foundedExtentionEventsWeb)
        {
            var eventDto = new EventDto
            {
                Id = foundedExtentionEventsWeb.Id,
                Banner = foundedExtentionEventsWeb.Banner,
                Date = foundedExtentionEventsWeb.Date,
                Name = foundedExtentionEventsWeb.Name,
                Description = foundedExtentionEventsWeb.Description,
                VenueInfoKey = foundedExtentionEventsWeb.VenueInfoKey
            };

            return eventDto;
        }
    }
}
