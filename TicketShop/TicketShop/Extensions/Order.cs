﻿using System.Collections.Generic;
using System.Linq;
using TicketShop.Services.ModelsDto;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Extensions
{
    public static class Order
    {
        public static List<MyOrdersWeb> OrderDtoToMyOrderWeb(this List<OrderDto> orderDtoType, List<OrderDto> foundedOrdersDto, List<TicketDto> tickets)
        {
            List<MyOrdersWeb> myOrdersWebs = new List<MyOrdersWeb>();

            foreach (var orderDto in foundedOrdersDto)
            {
                TicketDto ticket = tickets.FirstOrDefault(x => x.Id == orderDto.TicketInfoKey);

                MyOrdersWeb myOrdersWeb = new MyOrdersWeb
                {
                    OrderStatus = orderDto.Status,
                    TrackNo = orderDto.TrackNo,
                    //todo may be null
                    Ticket = new TicketWeb
                    {
                        Id = ticket.Id,
                        Price = ticket.Price,
                        Event = new EventWeb
                        {
                            Name = ticket.Event.Name,
                            Date = ticket.Event.Date
                        },
                        SallerName = ticket.Seller.FirstName + " " + ticket.Seller.LastName,
                        SallerEmail = ticket.Seller.Email,
                        SallerPhone = ticket.Seller.PhoneNumber,
                        SellerNotes = ticket.SellerNotes
                    }
                };

                myOrdersWebs.Add(myOrdersWeb);
            }

            return myOrdersWebs;
        }
    }
}
