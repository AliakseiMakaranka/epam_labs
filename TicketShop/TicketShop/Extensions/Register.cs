﻿using System;
using System.Collections.Generic;
using TicketShop.DataAccess.Enums;
using TicketShop.Services.ModelsDto;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Extensions
{
    public static class Register
    {
        public static UserDto RegisterWebToUserDto(this RegisterWeb registerWeb, RegisterWeb model, UserRoleEnum role)
        {
            UserDto userDto = new UserDto
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Localization = model.Localization,
                Address = model.Address,
                PhoneNumber = model.PhoneNumber,
                Email = model.Email,
                Password = model.Password
            };

            //Создаем нового человека
            List<UserRoleDto> userRoleDtos = new List<UserRoleDto>();

            userRoleDtos.Add(new UserRoleDto
            {
                User = userDto,
                UserId = userDto.Id,
                Role = new RoleDto
                {
                    Id = (int)role,
                    Name = Enum.GetName(typeof(UserRoleEnum), role)
                },
                RoleId = (int)role
            });
            userDto.UserRoles = userRoleDtos;

            return userDto;
        }
    }
}
