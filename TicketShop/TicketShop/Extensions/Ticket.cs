﻿using System.Collections.Generic;
using System.Linq;
using TicketShop.DataAccess.Enums;
using TicketShop.Services.ModelsDto;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Extensions
{
    public static class Ticket
    {
        //list
        public static List<TicketWeb> TicketsDtoToTicketsWeb(this List<TicketDto> ticketDtoType,
            List<TicketDto> ticketsDto)
        {
            List<TicketWeb> ticketsWeb = new List<TicketWeb>();

            foreach (var ticketDto in ticketsDto)
            {
                var ticketWeb = new TicketWeb
                {
                    Id = ticketDto.Id,
                    Price = ticketDto.Price,
                    EventDescription = ticketDto.Event.Description,
                    SallerEmail = ticketDto.Seller.Email,
                    SallerName = ticketDto.Seller.FirstName + " " + ticketDto.Seller.LastName,
                    SallerPhone = ticketDto.Seller.PhoneNumber,
                    SellerNotes = ticketDto.SellerNotes,
                    Event = new EventWeb
                    {
                        Date = ticketDto.Event.Date
                    }
                };

                ticketsWeb.Add(ticketWeb);
            }

            return ticketsWeb;
        }

        public static List<MyTicketsWeb> TicketsDtoToMyTicketsWeb(this List<TicketDto> ticketDtoType,
            List<TicketDto> foundedTicketsDto, List<OrderDto> orders)
        {
            List<MyTicketsWeb> myTicketsWeb = new List<MyTicketsWeb>();

            foreach (var ticketDto in foundedTicketsDto)
            {
                OrderDto order = orders.FirstOrDefault(x => x.TicketInfoKey == ticketDto.Id);

                string orderStatus = order != null ? order.Status : StatusEnum.Selling.ToString();

                if (order != null)
                {
                    var myTicketWeb = new MyTicketsWeb
                    {
                        TicketId = ticketDto.Id,
                        TicketPrice = ticketDto.Price,
                        EventDate = ticketDto.Event.Date.ToString("MMMM dd, yyyy"),
                        EventName = ticketDto.Event.Name,
                        OrderStatus = orderStatus,
                        BuyerPhone = order.Buyer.PhoneNumber,
                        BuyerName = order.Buyer.FirstName + " " + order.Buyer.LastName,
                        BuyerEmail = order.Buyer.Email
                    };
                    myTicketsWeb.Add(myTicketWeb);
                }
                else
                {
                    var myTicketWebWithoutOrder = new MyTicketsWeb
                    {
                        TicketId = ticketDto.Id,
                        TicketPrice = ticketDto.Price,
                        EventDate = ticketDto.Event.Date.ToString(),
                        EventName = ticketDto.Event.Name,
                        OrderStatus = orderStatus
                    };
                    myTicketsWeb.Add(myTicketWebWithoutOrder);
                }
            }

            return myTicketsWeb;
        }

        //Repeat for one
        public static TicketWeb TicketsDtoToTicketsWeb(this TicketDto ticketDtoType, TicketDto ticketsDto)
        {
            var ticketWeb = new TicketWeb
            {
                Id = ticketsDto.Id,
                Price = ticketsDto.Price,
                EventDescription = ticketsDto.Event.Description,
                SallerEmail = ticketsDto.Seller.Email,
                SallerName = ticketsDto.Seller.FirstName + " " + ticketsDto.Seller.LastName,
                SallerPhone = ticketsDto.Seller.PhoneNumber,
                SellerNotes = ticketsDto.SellerNotes
            };

            return ticketWeb;
        }

        public static MyTicketsWeb TicketsDtoToMyTicketsWeb(this TicketDto ticketDtoType,
            TicketDto foundedTicketsDto)
        {
            var myTicketWeb = new MyTicketsWeb
            {
                TicketId = foundedTicketsDto.Id,
                TicketPrice = foundedTicketsDto.Price
            };

            return myTicketWeb;
        }
    }
}
