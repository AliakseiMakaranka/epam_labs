﻿using System.Collections.Generic;
using TicketShop.Services.ModelsDto;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Extensions
{
    public static class User
    {
        public static UserWeb UserDtoToUserWeb(this UserDto userDto, UserDto foundedUserDto)
        {
            UserWeb result = new UserWeb
            {
                FirstName = foundedUserDto.FirstName,
                LastName = foundedUserDto.LastName,
                Localization = foundedUserDto.Localization,
                Address = foundedUserDto.Address,
                PhoneNumber = foundedUserDto.PhoneNumber,
                Email = foundedUserDto.Email,
                Password = foundedUserDto.Password
            };

            List<UserRoleWeb> userRoleWebs = new List<UserRoleWeb>();

            foreach (var roleDto in foundedUserDto.UserRoles)
            {
                var userRoleWeb = new UserRoleWeb
                {
                    Role = new RoleWeb
                    {
                        Id = roleDto.Role.Id,
                        Name = roleDto.Role.Name
                    }
                };

                userRoleWebs.Add(userRoleWeb);
            }

            result.UserRoles = userRoleWebs;

            return result;
        }

        public static List<UserWeb> UserDtoToUserWeb(this List<UserDto> userDto, List<UserDto> foundedUsersDto)
        {
            List<UserWeb> usersWeb = new List<UserWeb>();

            foreach (var foundedUserDto in foundedUsersDto)
            {
                UserWeb result = new UserWeb
                {
                    Id = foundedUserDto.Id,
                    FirstName = foundedUserDto.FirstName,
                    LastName = foundedUserDto.LastName,
                    Localization = foundedUserDto.Localization,
                    Address = foundedUserDto.Address,
                    PhoneNumber = foundedUserDto.PhoneNumber,
                    Email = foundedUserDto.Email,
                    Password = foundedUserDto.Password
                };

                List<UserRoleWeb> userRoleWebs = new List<UserRoleWeb>();

                foreach (var roleDto in foundedUserDto.UserRoles)
                {
                    var userRoleWeb = new UserRoleWeb
                    {
                        Role = new RoleWeb
                        {
                            Id = roleDto.Role.Id,
                            Name = roleDto.Role.Name
                        },
                        RoleId = roleDto.RoleId,
                        UserId = roleDto.UserId
                    };

                    userRoleWebs.Add(userRoleWeb);
                }

                result.UserRoles = userRoleWebs;

                usersWeb.Add(result);
            }

            return usersWeb;
        }


        public static LoginWeb UserDtoToLoginWeb(this UserDto userDto, UserDto foundedUserDto)
        {
            List<UserRoleWeb> userRolesWeb = new List<UserRoleWeb>();

            foreach (var roleDto in foundedUserDto.UserRoles)
            {
                var userRoleWeb = new UserRoleWeb
                {
                    RoleId = roleDto.RoleId,
                    UserId = roleDto.UserId,
                    Role = new RoleWeb
                    {
                        Id = roleDto.Role.Id,
                        Name = roleDto.Role.Name
                    },
                    User = new UserWeb
                    {
                        Id = roleDto.User.Id
                    }
                };

                userRolesWeb.Add(userRoleWeb);
            }

            var loginWeb = new LoginWeb
            {
                Id = foundedUserDto.Id,
                Email = foundedUserDto.Email,
                Localization = foundedUserDto.Localization,
                Password = foundedUserDto.Password,
                UserRoles = userRolesWeb
            };

            return loginWeb;
        }
    }
}
