﻿using System.Collections.Generic;
using TicketShop.Services.ModelsDto;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.Extensions
{
    public static class Venue
    {
        //lists
        public static List<VenueWeb> VenueDtoToVenueWeb(this List<VenueDto> venueDtoType,
            List<VenueDto> foundedvenuesDto)
        {
            List<VenueWeb> venuesWeb = new List<VenueWeb>();

            foreach (var venueDto in foundedvenuesDto)
            {
                var venueWeb = new VenueWeb
                {
                    Id = venueDto.Id,
                    Name = venueDto.Name,
                    Address = venueDto.Address,
                    City = new CityWeb
                    {
                        Id = venueDto.City.Id,
                        Name = venueDto.City.Name
                    }
                };
                venuesWeb.Add(venueWeb);
            }

            return venuesWeb;
        }

        public static List<VenueDto> VenueWebToVenueDto(this List<VenueWeb> venueWebType,
            List<VenueWeb> foundedvenuesWeb)
        {
            List<VenueDto> venuesDto = new List<VenueDto>();

            foreach (var venueWeb in foundedvenuesWeb)
            {
                var venueDto = new VenueDto
                {
                    Id = venueWeb.Id,
                    Name = venueWeb.Name,
                    Address = venueWeb.Address,
                    City = new CityDto
                    {
                        Id = venueWeb.City.Id,
                        Name = venueWeb.City.Name
                    }
                };

                venuesDto.Add(venueDto);
            }

            return venuesDto;
        }

        public static List<VenueDto> ExtentionVenueWebToVenueDto(
            this List<ExtentionVenueWeb> extentionVenueWebType,
            List<ExtentionVenueWeb> foundedExtentionVenuesWeb)
        {
            List<VenueDto> venuesDto = new List<VenueDto>();

            foreach (var extentionVenueWeb in foundedExtentionVenuesWeb)
            {
                var venueDto = new VenueDto
                {
                    Id = extentionVenueWeb.Id,
                    Name = extentionVenueWeb.Name,
                    Address = extentionVenueWeb.Address,
                    CityInfoKey = extentionVenueWeb.CityInfoKey
                };

                venuesDto.Add(venueDto);
            }

            return venuesDto;
        }

        public static List<ExtentionVenueWeb> VenueDtoToExtentionVenueWeb(this List<VenueDto> venueDtoType,
            List<VenueDto> foundedVenuesDto)
        {
            List<ExtentionVenueWeb> extentionVenueWebs = new List<ExtentionVenueWeb>();

            foreach (var foundedVenueDto in foundedVenuesDto)
            {
                var venueDto = new ExtentionVenueWeb
                {
                    Id = foundedVenueDto.Id,
                    Name = foundedVenueDto.Name,
                    Address = foundedVenueDto.Address,
                    CityInfoKey = foundedVenueDto.CityInfoKey
                };

                extentionVenueWebs.Add(venueDto);
            }

            return extentionVenueWebs;
        }

        //Repeat for one
        public static VenueWeb VenueDtoToVenueWeb(this VenueDto venueDtoType, VenueDto foundedvenuesDto)
        {
            var venueWeb = new VenueWeb
            {
                Id = foundedvenuesDto.Id,
                Name = foundedvenuesDto.Name,
                Address = foundedvenuesDto.Address,
                City = new CityWeb
                {
                    Id = foundedvenuesDto.City.Id,
                    Name = foundedvenuesDto.City.Name
                }
            };

            return venueWeb;
        }

        public static VenueDto VenueWebToVenueDto(this VenueWeb venueWebType, VenueWeb foundedvenuesWeb)
        {
            var venueDto = new VenueDto
            {
                Id = foundedvenuesWeb.Id,
                Name = foundedvenuesWeb.Name,
                Address = foundedvenuesWeb.Address,
                City = new CityDto
                {
                    Id = foundedvenuesWeb.City.Id,
                    Name = foundedvenuesWeb.City.Name
                }
            };

            return venueDto;
        }

        public static VenueDto ExtentionVenueWebToVenueDto(this ExtentionVenueWeb extentionVenueWebType,
            ExtentionVenueWeb foundedExtentionVenuesWeb)
        {
            var venueDto = new VenueDto
            {
                Id = foundedExtentionVenuesWeb.Id,
                Name = foundedExtentionVenuesWeb.Name,
                Address = foundedExtentionVenuesWeb.Address,
                CityInfoKey = foundedExtentionVenuesWeb.CityInfoKey
            };

            return venueDto;
        }

        public static ExtentionVenueWeb VenueDtoToExtentionVenueWeb(this VenueDto venueDtoType,
            VenueDto foundedVenuesDto)
        {
            var venueDto = new ExtentionVenueWeb
            {
                Id = foundedVenuesDto.Id,
                Name = foundedVenuesDto.Name,
                Address = foundedVenuesDto.Address,
                CityInfoKey = foundedVenuesDto.CityInfoKey
            };

            return venueDto;
        }
    }
}
