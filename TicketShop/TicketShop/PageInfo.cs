﻿using System.Runtime.Serialization;
using System;

namespace TicketShop.Web
{
    [DataContract(Name = "PageInfo", Namespace = "")]
    public class PageInfo
    {
        [DataMember]
        public int PageNumber { get; set; } // номер текущей страницы
        [DataMember]
        public int PageSize { get; set; } // кол-во объектов на странице
        [DataMember]
        public int TotalItems { get; set; } // всего объектов

        // всего страниц
        //public int TotalPages => (int)Math.Ceiling((decimal)TotalItems / PageSize);
    }
}