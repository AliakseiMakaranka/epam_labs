﻿using System.ComponentModel.DataAnnotations;

namespace TicketShop.Web.ViewModels
{
    public class CityWeb
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "City name is not specified")]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "The length of the string must be between 2 and 30 characters")]
        public string Name { get; set; }
    }
}