﻿namespace TicketShop.Web.ViewModels
{
    public class ConfirmationTicketBuy
    {
        public int TicketId { get; set; }

        public string Comment { get; set; }

        public string TrackNo { get; set; }
    }
}