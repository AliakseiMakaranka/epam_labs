﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TicketShop.Web.ViewModels
{
    public class CreateTicketWeb
    {
        public List<EventWeb> EventsWeb { get; set; }

        public int EventInfoKey { get; set; }
        
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Event is not specified")]
        [DataType(DataType.Text)]
        public string EventName { get; set; }
    }
}
