﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TicketShop.Web.ViewModels
{
    [DataContract(Name = "EventWeb", Namespace = "")]
    public class EventWeb
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public string Banner { get; set; }
        [DataMember]
        public string CityName { get; set; }
        [DataMember]
        public string VenueName { get; set; }
        [DataMember]
        public List<int> AvailableTickets { get; set; }
        [DataMember]
        public int CityId { get; set; }
    }
}
