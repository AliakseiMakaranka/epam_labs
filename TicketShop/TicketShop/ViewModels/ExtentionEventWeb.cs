﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TicketShop.Web.ViewModels
{
    public class ExtentionEventWeb
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Event name is not specified")]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "The length of the string must be between 2 and 30 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Venue name is not specified")]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "The length of the string must be between 2 and 30 characters")]
        public string VenueName { get; set; }

        [Required(ErrorMessage = "Description is not specified")]
        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "The length of the string must be between 2 and 50 characters")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Description is not specified")]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        public string Banner { get; set; }

        public VenueWeb Venue { get; set; }

        public int VenueInfoKey { get; set; }

        public List<ExtentionVenueWeb> AllVenues { get; set; }
    }
}
