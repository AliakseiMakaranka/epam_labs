﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TicketShop.Web.ViewModels
{
    public class ExtentionVenueWeb
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Venue name is not specified")]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "The length of the string must be between 2 and 30 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Address is not specified")]
        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "The length of the string must be between 2 and 50 characters")]
        public string Address { get; set; }

        public int CityInfoKey { get; set; }

        public string CityNameInput { get; set; }

        public List<CityWeb> AllCities { get; set; }
    }
}
