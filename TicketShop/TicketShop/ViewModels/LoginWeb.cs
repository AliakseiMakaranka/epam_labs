﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TicketShop.Web.ViewModels
{
    public class LoginWeb
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Email is not specified")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is not specified")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public List<UserRoleWeb> UserRoles { get; set; }

        public string Localization { get; set; }
    }
}
