﻿namespace TicketShop.Web.ViewModels
{
    public class MyOrdersWeb
    {
        public TicketWeb Ticket { get; set; }

        public string OrderStatus { get; set; }

        public string TrackNo { get; set; }
    }
}
