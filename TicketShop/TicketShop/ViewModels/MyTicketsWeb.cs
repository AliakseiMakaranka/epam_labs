﻿namespace TicketShop.Web.ViewModels
{
    public class MyTicketsWeb
    {
        public string EventName { get; set; }

        public int TicketId { get; set; }

        public decimal TicketPrice { get; set; }

        public string OrderStatus { get; set; }

        public string EventDate { get; set; }

        public string BuyerName { get; set; }

        public string BuyerEmail { get; set; }

        public string BuyerPhone { get; set; }
    }
}
