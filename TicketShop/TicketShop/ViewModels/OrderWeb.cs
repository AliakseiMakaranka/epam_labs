﻿using System.Collections.Generic;

namespace TicketShop.Web.ViewModels
{
    public class OrderWeb
    {
        public List<TicketWeb> Tickets { get; set; }

        public string Status { get; set; }
    }
}
