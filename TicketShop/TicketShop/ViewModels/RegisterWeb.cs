﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TicketShop.Web.ViewModels
{
    public class RegisterWeb
    {
        [Required(ErrorMessage = "First name is not specified")]
        [DataType(DataType.Text)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Second name is not specified")]
        [DataType(DataType.Text)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Localization is not specified")]
        [DataType(DataType.Text)]
        public string Localization { get; set; }

        [Required(ErrorMessage = "Address is not specified")]
        [DataType(DataType.Text)]
        public string Address { get; set; }

        [Required(ErrorMessage = "Phone number is not specified")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Email is not specified")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is not specified")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "The length of the string must be between 5 and 20 characters")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Incorrectly password")]
        public string ConfirmPassword { get; set; }

        public SelectList LanguageList { get; set; }
    }
}
