﻿namespace TicketShop.Web.ViewModels
{
    public class RoleWeb
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
