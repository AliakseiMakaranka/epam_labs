﻿namespace TicketShop.Web.ViewModels
{
    public class TicketWeb
    {
        public int Id { get; set; }

        public decimal Price { get; set; }

        public string SallerName { get; set; }

        public string SallerEmail { get; set; }

        public string SallerPhone { get; set; }

        public string SellerNotes { get; set; }

        public string EventDescription { get; set; }

        public OrderWeb Order { get; set; }

        public EventWeb Event { get; set; }
    }
}
