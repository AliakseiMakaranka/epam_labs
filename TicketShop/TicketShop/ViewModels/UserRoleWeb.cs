﻿namespace TicketShop.Web.ViewModels
{
    public class UserRoleWeb
    {
        public int UserId { get; set; }

        public UserWeb User { get; set; }

        public int RoleId { get; set; }

        public RoleWeb Role { get; set; }
    }
}
