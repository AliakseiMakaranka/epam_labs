﻿using System.ComponentModel.DataAnnotations;

namespace TicketShop.Web.ViewModels

{
    public class VenueWeb
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string Address { get; set; }

        public int CityInfoKey { get; set; }

        public CityWeb City { get; set; }
    }
}