﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using TicketShop.Web.ViewModels;

namespace TicketShop.Web.ViewModelsPages
{
    [DataContract(Name = "EventWebPages", Namespace = "")]
    public class EventWebPages
    {
        [DataMember]
        public List<EventWeb> EventWebs { get; set; }

        [DataMember]
        public PageInfo PageInfo { get; set; }
    }
}
