var url = '/api/event/?';
var eventCount = 0;

window.onload = function () {
    getEventCount();

    function getEventCount() {
        $.ajax({
            url: url,
            type: 'GET',
            contentType: "application/json",
            success: function (_events) {
                $.each(_events,
                    function (index, event) {
                        eventCount += 1;
                    });
            }
        });
    }
}

$(document).ready(function () {
    GetEvents();

    $("#eventName").change(function () {
        url = url.replace(/pageSize=\w+&? */g, '');
        url = url.replace(/pageNumber=\w+&? */g, '');
        url = url.replace(/eventName=.+&? */g, '');
        document.getElementById("pageSize").value = 0;
        document.getElementById("pageNumber").value = 1;
        url = url.concat('pageSize=0&pageNumber=1&');
        var eventName = 'eventName='.concat(document.getElementById('eventName').value);
        url = url.concat(eventName).concat('&');
        GetEvents();
    });

    $('body').on('change', '[id="from"]', function () {
        url = url.replace(/beginDate=.+&? */g, '');
        GetEvents();
    });

    $('body').on('change', '[id="to"]', function () {
        url = url.replace(/endDate=.+&? */g, '');
        GetEvents();
    });

    $("#from").datepicker({
        onSelect: function (dateText) {
            url = url.replace(/beginDate=.+&? */g, '');
            var dateAsString = dateText;
            var beginDate = 'beginDate='.concat(dateAsString);
            url = url.concat(beginDate).concat('&');
            GetEvents();
        }
    });

    $("#to").datepicker({
        onSelect: function (dateText) {
            url = url.replace(/endDate=.+&? */g, '');
            var dateAsString = dateText;
            var endDate = 'endDate='.concat(dateAsString);
            url = url.concat(endDate).concat('&');
            GetEvents();
        }
    });

    $('body').on('change', '*[id^="venue_"]', function () {
        url = url.replace(/venuesIds=\w+&? */g, '');
        $('input[id^="venue_"]:checked').each(function () {
            var values = 'venuesIds='.concat(this.value);
            url = url.concat(values).concat('&');
        });
        url = url.replace(/pageSize=\w+&? */g, '');
        url = url.replace(/pageNumber=\w+&? */g, '');
        url = url.concat('pageSize=0&pageNumber=1&');
        document.getElementById("pageSize").value = 0;
        document.getElementById("pageNumber").value = 1;
        GetEvents();
    });

    $('body').on('change', '*[id^="city_"]', function () {
        url = url.replace(/citiesIds=\w+&? */g, '');
        $('input[id^="city_"]:checked').each(function () {
            var values = 'citiesIds='.concat(this.value);
            url = url.concat(values).concat('&');

        });
        url = url.replace(/pageSize=\w+&? */g, '');
        url = url.replace(/pageNumber=\w+&? */g, '');
        url = url.concat('pageSize=0&pageNumber=1&');
        document.getElementById("pageSize").value = 0;
        document.getElementById("pageNumber").value = 1;
        GetEvents();
    });

    $("#pageSize").change(function () {
        url = url.replace(/pageSize=\w+&? */g, '');
        url = url.replace(/pageNumber=\w+&? */g, '');
        document.getElementById('pageNumber').value = 1;
        url = url.concat('pageNumber = 1').concat('&');
        pageSize = 'pageSize='.concat(document.getElementById("pageSize").value);
        url = url.concat(pageSize).concat('&');
        GetEvents();
    });

    $("#pageNumber").change(function () {
        pageNumber = document.getElementById('pageNumber').value;
        url = url.replace(/pageNumber=\w+&? */g, '');
        var pageNumber = 'pageNumber='.concat(document.getElementById('pageNumber').value);
        url = url.concat(pageNumber).concat('&');
        GetEvents();
    });

    document.getElementById("back").onclick = function () {
        var page = parseInt(document.getElementById('pageNumber').value);
        if (page > 1) {
            document.getElementById('pageNumber').value = page - 1;
            pageNumber = document.getElementById('pageNumber').value;
            url = url.replace(/pageNumber=\w+&? */g, '');
            var pageNumber = 'pageNumber='.concat(document.getElementById('pageNumber').value);
            url = url.concat(pageNumber).concat('&');
            GetEvents();
        }
    }

    document.getElementById("forward").onclick = function () {
        if (document.getElementById("pageSize").value != 0) {
            var page = parseInt(document.getElementById('pageNumber').value);
            var maxPages = eventCount / document.getElementById("pageSize").value;
            if (page < maxPages) {
                document.getElementById('pageNumber').value = page + 1;
                document.getElementById('pageNumber').value = page + 1;
                pageNumber = document.getElementById('pageNumber').value;
                url = url.replace(/pageNumber=\w+&? */g, '');
                var pageNumber = 'pageNumber='.concat(document.getElementById('pageNumber').value);
                url = url.concat(pageNumber).concat('&');
                GetEvents();
            }
        }
    }

    function GetEvents() {
        $.ajax({
            url: url,
            headers: {
                Accept: "application/json"
                //Accept: "application/xml"
                //Accept: "text/csv"
            },
            type: 'GET',

            success: function (_events, textStatus, request) {
                var responseHeaders = request.getAllResponseHeaders(),
                    xmlAcceptType = "application/xml";
                csvAcceptType = "text/csv";

                //-----------------������ ���� ������ xml---------------
                if (responseHeaders.indexOf(xmlAcceptType) !== -1) {
                    _events = xmlToJson(_events);
                }

                //-----------------������ ���� ������ csv---------------
                if (responseHeaders.indexOf(csvAcceptType) !== -1) {
                    _events = csvToJson(_events);
                }

                var rows = "";
                var venues = "";
                var cities = "";
                var events = "";
                //������ ������ ������ �������
                var myTable = document.getElementById("myTable");
                for (var x = myTable.rows.length - 1; x > 0; x--) {
                    myTable.deleteRow(x);
                }

                $.each(_events,
                    function (index, event) {
                        rows += getRowHtml(event);
                        venues += getVenueHtml(event);
                        cities += getCityHtml(event);
                        events += getEventHtml(event);
                    });
                $("table tbody").append(rows);

                if ($('#venueCheckboxes').is(':empty')) {
                    if (true) {
                        document.getElementById('venueCheckboxes').innerHTML += venues;
                        document.getElementById('cityCheckboxes').innerHTML += cities;
                        document.getElementById('eventLists').innerHTML += events;
                    }
                }
            }
        });
    }
    
    var getVenueHtml = function (event) { return "<input type='checkbox' id=venue_" + event.id + " value=" + event.id + ">" + event.venueName; }
    var getCityHtml = function (event) { return "<input type='checkbox' id='city_" + event.cityId + "' value=" + event.cityId + ">" + event.cityName; }
    var getEventHtml = function (event) { return "<option value=" + event.name + "></option>"; }
});


var xmlToJson = function (_events) {
    var temp = [];
    $(_events).find('EventWeb').each(function () {
        var sBanner = $(this).find('Banner').text();
        var sCityId = $(this).find('CityId').text();
        var sCityName = $(this).find('CityName').text();
        var sDate = $(this).find('Date').text();
        var sId = $(this).find('Id').text();
        var sName = $(this).find('Name').text();
        var sVenueName = $(this).find('VenueName').text();
        temp.push(
            {
                "id": sId,
                "name": sName,
                "date": sDate,
                "banner": sBanner,
                "cityName": sCityName,
                "venueName": sVenueName,
                "availableTickets": null,
                "cityId": sCityId
            }
        );
    });

    return temp;
}

var csvToJson = function (_events) {
    var temp = [];
    var lines = _events.split("\n");
    var result = [];
    var headers = lines[0].split(";");

    var newHeaders = [];
    $.each(headers,
        function (index, header) {
            newHeaders.push(header.charAt(0).toLowerCase() + header.substr(1));
        });

    for (var i = 1; i < lines.length - 1; i++) {
        var obj = {};
        var currentline = lines[i].split(";");
        for (var j = 0; j < newHeaders.length; j++) {
            obj[newHeaders[j]] = currentline[j];
        }

        result.push(obj);
    }

    return result;
}

var getRowHtml = function (event) {
    event.banner = event.banner.replace("~", "..");

    return "<tr data-rowid='" + event.id + "'>" +
        "<td>" + event.id + "</td>" +
        "<td> <img src=" + event.banner + "  height='150px' width='250px'/> </td>" +
        "<td>" + event.name + "</td>" +
        "<td>" + event.date + "</td>" +
        "<td>" + event.cityName + "</td>" +
        "<td>" + event.venueName + "</td>" +
        "</tr>";
}