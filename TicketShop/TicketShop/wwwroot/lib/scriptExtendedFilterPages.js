var url = '/api/eventPages/?';

var eventWebs = [];
var pageNumber;
var pageSize;
var totalItems;
var selectedVenues = [];
var selectedCities = [];

$(document).ready(function () {
    $('#eventName').select2();
    $('#venueCheckboxes').select2({ allowClear: true });
    $('#cityCheckboxes').select2({ allowClear: true });

    GetEvents();

    document.getElementById("forward").style.cssText = "visibility: hidden; ";
    document.getElementById("back").style.cssText = "visibility: hidden; ";
    document.getElementById("pageNumber").style.cssText = "visibility: hidden; ";

    $("#eventName").change(function () {
        clearPageSizeAndNumber();
        url = url.replace(/eventName=.+&? */g, '');
        var eventName = 'eventName='.concat(document.getElementById('eventName').value);
        if (eventName === "eventName=All") {
            eventWebs = null;
            eventName = "eventName =";

            //clear dropbox and reInit with use ajax
            var select = document.getElementById("eventName");
            var count = select.options.length;

            for (i = 1; i <= count; i++) {
                select.options[1] = null;
            }
            //---------------------------------------
        }
        url = url.concat(eventName).concat('&');
        GetEvents();
    });

    //choose date
    $("#from").datepicker({
        onSelect: function (dateText) {
            url = url.replace(/beginDate=.+&? */g, '');
            var dateAsString = dateText;
            var beginDate = 'beginDate='.concat(dateAsString);
            url = url.concat(beginDate).concat('&');
            GetEvents();
        }
    });

    $("#to").datepicker({
        onSelect: function (dateText) {
            url = url.replace(/endDate=.+&? */g, '');
            var dateAsString = dateText;
            var endDate = 'endDate='.concat(dateAsString);
            url = url.concat(endDate).concat('&');
            GetEvents();
        }
    });

    //clear date icon
    $('#date_delete_from').click(function () {
        url = url.replace(/beginDate=.+&? */g, '');

        $("#from").datepicker({
        }).attr('readonly', 'readonly');
        $("#from").datepicker("setDate", "");
    });

    $('#date_delete_to').click(function () {
        url = url.replace(/endDate=.+&? */g, '');

        $("#to").datepicker({
        }).attr('readonly', 'readonly');
        $("#to").datepicker("setDate", "");
    });

    $("#venueCheckboxes").change(function () {
        selectedVenues = $("#venueCheckboxes").select2('data').map(function (a) { return a.id; });

        url = url.replace(/venuesIds=\w+&? */g, '');

        selectedVenues.forEach(function (item, index) {
            var values = 'venuesIds='.concat(item);
            url = url.concat(values).concat('&');
        });
        clearPageSizeAndNumber();
        GetEvents();
    });

    $("#cityCheckboxes").change(function () {
        selectedVenues = $("#cityCheckboxes").select2('data').map(function (a) { return a.id; });

        url = url.replace(/citiesIds=\w+&? */g, '');

        selectedVenues.forEach(function (item, index) {
            var values = 'citiesIds='.concat(item);
            url = url.concat(values).concat('&');
        });
        clearPageSizeAndNumber();
        GetEvents();
    });

    $('#clear_all_filters').click(function () {
        url = '/api/eventPages/?';

        document.getElementById('eventName').value = "All";
        document.getElementById('select2-eventName-container').innerHTML = "All";

        $("#from").datepicker({
        }).attr('readonly', 'readonly');
        $("#from").datepicker("setDate", "");

        $("#to").datepicker({
        }).attr('readonly', 'readonly');
        $("#to").datepicker("setDate", "");

        $("#venueCheckboxes").empty().trigger('change');
        $("#cityCheckboxes").empty().trigger('change');

        GetEvents();
    });

    $("#pageSize").change(function () {
        url = url.replace(/pageSize=\w+&? */g, '');
        url = url.replace(/pageNumber=\w+&? */g, '');
        document.getElementById('pageNumber').value = 1;
        url = url.concat('pageNumber = 1').concat('&');
        pageSize = 'pageSize='.concat(document.getElementById("pageSize").value);

        if (document.getElementById("pageSize").value != 0) {
            document.getElementById("forward").style.cssText = "visibility: visible; ";
            document.getElementById("back").style.cssText = "visibility: visible; ";
            document.getElementById("pageNumber").style.cssText = "visibility: visible; ";
        } else {
            document.getElementById("forward").style.cssText = "visibility: hidden; ";
            document.getElementById("back").style.cssText = "visibility: hidden; ";
            document.getElementById("pageNumber").style.cssText = "visibility: hidden; ";
        }

        url = url.concat(pageSize).concat('&');
        GetEvents();
    });

    $("#pageNumber").change(function () {
        var manualPageNumber = document.getElementById('pageNumber').value;

        var maxPages = totalItems / pageSize;

        if (manualPageNumber <= maxPages) {
            url = url.replace(/pageNumber=\w+&? */g, '');

            url = url.concat(manualPageNumber).concat('&');
            GetEvents();
        } else {
            alert("Sorry, the page not found.");
            document.getElementById('pageNumber').value = pageNumber;
        }
    });

    function clearPageSizeAndNumber() {
        url = url.replace(/pageNumber=\w+&? */g, '');
        url = url.concat('pageNumber=1&');
        document.getElementById("pageNumber").value = 1;
    }

    document.getElementById("back").onclick = function () {
        var pageNumber = parseInt(document.getElementById('pageNumber').value);
        if (pageNumber > 1) {
            document.getElementById('pageNumber').value = pageNumber - 1;
            pageNumber = document.getElementById('pageNumber').value;
            url = url.replace(/pageNumber=\w+&? */g, '');
            var pageNumber = 'pageNumber='.concat(document.getElementById('pageNumber').value);
            url = url.concat(pageNumber).concat('&');
            GetEvents();
        }
    }

    document.getElementById("forward").onclick = function () {
        if (document.getElementById("pageSize").value != 0) {
            var maxPages = totalItems / pageSize;
            pageNumber = document.getElementById("pageNumber").value;
            if (pageNumber < maxPages) {
                pageNumber = parseInt(pageNumber) + 1;
                document.getElementById("pageNumber").value = pageNumber;
                url = url.replace(/pageNumber=\w+&? */g, '');
                url = url.concat('pageNumber='.concat(pageNumber)).concat('&');
                GetEvents();
            }
        }
    }
});




function GetEvents() {
    $.ajax({
        url: url,
        headers: {
            Accept: "application/json"
            //Accept: "application/xml"
            //Accept: "text/csv"
        },
        type: 'GET',
        success: function (response, textStatus, request) {
            var responseHeaders = request.getAllResponseHeaders(),
                xmlAcceptType = "application/xml";
            csvAcceptType = "text/csv";
            jsonAcceptType = "application/json";

            //-------------------���� ������ json-----------------
            if (responseHeaders.indexOf(jsonAcceptType) !== -1) {
                eventWebs = response.eventWebs;
                pageNumber = response.pageInfo.pageNumber;
                pageSize = response.pageInfo.pageSize;
                totalItems = response.pageInfo.totalItems;
            }

            //-----------------������ ���� ������ xml---------------
            if (responseHeaders.indexOf(xmlAcceptType) !== -1) {
                eventWebs = xmlToJson(response);

                $(response).find('PageInfo').each(function () {
                    pageNumber = $(this).find('PageNumber').text();
                    pageSize = $(this).find('PageSize').text();
                    totalItems = $(this).find('TotalItems').text();
                });
            }

            //-----------------������ ���� ������ csv---------------
            if (responseHeaders.indexOf(csvAcceptType) !== -1) {
                eventWebs = response.eventWebs;
                pageNumber = response.pageInfo.pageNumber;
                pageSize = response.pageInfo.pageSize;
                totalItems = response.pageInfo.totalItems;

                eventWebs = csvToJson(response);
            }
            RenderPage();
        }
    });
}

function RenderPage() {
    var rows = "";
    //������ ������ ������ �������
    var myTable = document.getElementById("myTable");
    for (var x = myTable.rows.length - 1; x > 0; x--) {
        myTable.deleteRow(x);
    }

    $.each(eventWebs,
        function (index, event) {
            rows += showResults(event);

            $('#eventName').each(function () {
                if (this.value != event.name) {
                    $("#eventName")
                        .append($("<option></option>")
                            .attr("value", event.name)
                            .text(event.name));
                }
            });
            
            $("#venueCheckboxes")
                .append($("<option></option>")
                    .attr("value", event.id)
                    .text(event.venueName));

            $("#cityCheckboxes")
                .append($("<option></option>")
                    .attr("value", event.id)
                    .text(event.cityName));
        });
}

var xmlToJson = function (response) {

    var temp = [];
    $(response).find('EventWeb').each(function () {
        var sBanner = $(this).find('Banner').text();
        var sCityId = $(this).find('CityId').text();
        var sCityName = $(this).find('CityName').text();
        var sDate = $(this).find('Date').text();
        var sId = $(this).find('Id').text();
        var sName = $(this).find('Name').text();
        var sVenueName = $(this).find('VenueName').text();
        temp.push(
            {
                "id": sId,
                "name": sName,
                "date": sDate,
                "banner": sBanner,
                "cityName": sCityName,
                "venueName": sVenueName,
                "availableTickets": null,
                "cityId": sCityId
            }
        );
    });

    return temp;
}

var csvToJson = function (response) {
    var temp = [];
    var lines = eventWebs.split("\n");
    var result = [];
    var headers = lines[0].split(";");

    var newHeaders = [];
    $.each(headers,
        function (index, header) {
            newHeaders.push(header.charAt(0).toLowerCase() + header.substr(1));
        });

    for (var i = 1; i < lines.length - 1; i++) {
        var obj = {};
        var currentline = lines[i].split(";");
        for (var j = 0; j < newHeaders.length; j++) {
            obj[newHeaders[j]] = currentline[j];
        }

        result.push(obj);
    }

    return result;
}

//���������� ������� �� ������� �� �����
$(function () {
    $('#headings th').click(function () {
        var id = $(this).attr('id');
        var asc = (!$(this).attr('asc'));

        $('#headings th').each(function () {
            $(this).removeAttr('asc');
        });
        if (asc) $(this).attr('asc', 'asc');

        //������ ������
        var mySpan1 = document.getElementById(id).getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt")[0]
        var mySpan2 = document.getElementById(id).getElementsByClassName("glyphicon glyphicon-sort-by-attributes")[0]
        if (typeof mySpan1 !== "undefined") {
            mySpan1.className = "glyphicon glyphicon-sort-by-attributes";
        }
        if (typeof mySpan2 !== "undefined") {
            mySpan2.className = "glyphicon glyphicon-sort-by-attributes-alt";
        }

        sortResults(id, asc);
    });

    showResults();
});

function sortResults(prop, asc) {
    eventWebs = eventWebs.sort(function (a, b) {
        if (asc)
            return (a[prop] > b[prop]);
        else
            return (b[prop] > a[prop]);
    });
    showResults();
}

function showResults() {
    var html = '';
    for (var e in eventWebs) {
        eventWebs[e].banner = eventWebs[e].banner.replace("~", "..");

        html += '<tr>'
            + '<td>' + eventWebs[e].id + '</td>'
            + '<td>' + "<img src=" + eventWebs[e].banner + " height='150px' width='250px' />" + '</td>'
            + '<td>' + eventWebs[e].name + '</td>'
            + '<td>' + eventWebs[e].date + '</td>'
            + '<td>' + eventWebs[e].cityName + '</td>'
            + '<td>' + eventWebs[e].venueName + '</td>'
            + '<td>' + "<a , class='btn btn-primary' href='/Ticket/Tickets?eventId=" + eventWebs[e].id + "'> tap to buy ticket</a >" + '</td>'
            + '</tr>';
    }
    $('#results').html(html);
}