﻿using System;
using System.Collections.Generic;
using System.Windows;
using Caliburn.Micro;
using UrlPerformance_Tester.ViewModels;

namespace UrlPerformance_Tester
{
    public class Bootstrapper : BootstrapperBase
    {
        private readonly SimpleContainer _container = new SimpleContainer();

        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<IMainViewModel>();
        }

        protected override void Configure()
        {
            _container.RegisterInstance(typeof(IWindowManager), null, new WindowManager());
            _container.RegisterInstance(typeof(IMainViewModel), null, new MainViewModel());
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }
    }
}