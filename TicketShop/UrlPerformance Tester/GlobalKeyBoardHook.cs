﻿#region License_Do_Not_Remove
/* 
*  Made by TheDarkJoker94. 
*  Check http://thedarkjoker94.cer33.com/ for more C# Tutorials 
*  and also SUBSCRIBE to my Youtube Channel http://www.youtube.com/user/TheDarkJoker094  
*  GlobalKeyboardHook is licensed under a Creative Commons Attribution 3.0 Unported License.(http://creativecommons.org/licenses/by/3.0/)
*  This means you can use this Code for whatever you want as long as you credit me! That means...
*  DO NOT REMOVE THE LINES ABOVE !!! 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace UrlPerformance_Tester
{
    public class GlobalKeyboardHook
    {
        [DllImport("user32.dll")]
        static extern int CallNextHookEx(IntPtr hhk, int code, int wParam, ref KeyBoardHookStruct lParam);
        [DllImport("user32.dll")]
        static extern IntPtr SetWindowsHookEx(int idHook, LlKeyboardHook callback, IntPtr hInstance, uint theardId);
        [DllImport("user32.dll")]
        static extern bool UnhookWindowsHookEx(IntPtr hInstance);
        [DllImport("kernel32.dll")]
        static extern IntPtr LoadLibrary(string lpFileName);

        public delegate int LlKeyboardHook(int code, int wParam, ref KeyBoardHookStruct lParam);

        public struct KeyBoardHookStruct
        {
            public int VkCode;
            public int ScanCode;
            public int Flags;
            public int Time;
            public int DwExtraInfo;
        }

        const int WhKeyboardLl = 13;
        const int WmKeydown = 0x0100;
        const int WmKeyup = 0x0101;
        const int WmSyskeydown = 0x0104;
        const int WmSyskeyup = 0x0105;

        readonly LlKeyboardHook _llkh;
        public List<Keys> HookedKeys = new List<Keys>();

        IntPtr _hook = IntPtr.Zero;

        public event KeyEventHandler KeyDown;
        public event KeyEventHandler KeyUp;

        // This is the Constructor. This is the code that runs every time you create a new GlobalKeyboardHook object
        public GlobalKeyboardHook()
        {
            _llkh = HookProc;
            // This starts the hook. You can leave this as comment and you have to start it manually (the thing I do in the tutorial, with the button)
            // Or delete the comment mark and your hook will start automatically when your program starts (because a new GlobalKeyboardHook object is created)
            // That's why there are duplicates, because you start it twice! I'm sorry, I haven't noticed this...
            // hook(); <-- Choose!
        }
        ~GlobalKeyboardHook()
        { Unhook(); }

        public void Hook()
        {
            IntPtr hInstance = LoadLibrary("User32");
            _hook = SetWindowsHookEx(WhKeyboardLl, _llkh, hInstance, 0);
        }

        public void Unhook()
        {
            UnhookWindowsHookEx(_hook);
        }

        public int HookProc(int code, int wParam, ref KeyBoardHookStruct lParam)
        {
            if (code >= 0)
            {
                Keys key = (Keys)lParam.VkCode;
                if (HookedKeys.Contains(key))
                {
                    KeyEventArgs kArg = new KeyEventArgs(key);
                    if ((wParam == WmKeydown || wParam == WmSyskeydown) && KeyDown != null)
                        KeyDown(this, kArg);
                    else if ((wParam == WmKeyup || wParam == WmSyskeyup) && KeyUp != null)
                        KeyUp(this, kArg);
                    if (kArg.Handled)
                        return 1;
                }
            }
            return CallNextHookEx(_hook, code, wParam, ref lParam);
        }

    }
}

