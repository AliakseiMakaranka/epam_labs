﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Caliburn.Micro;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace UrlPerformance_Tester.ViewModels
{
    class MainViewModel : PropertyChangedBase
    {
        private const string DefaultUrl = "http://ya.ru";
        private const int DefaulThreadCount = 2;
        private const int DefaultRequestPerSec = 3;
        private const int DefaultDurationSec = 5;

        private readonly TimeSpan _requestTimeOut;

        public string Url { get; set; } = DefaultUrl;
        public int ThreadCount { get; set; } = DefaulThreadCount;
        public int RequestPerSec { get; set; } = DefaultRequestPerSec;
        public int DurationSec { get; set; } = DefaultDurationSec;
        private bool _isLockUi = true;

        //for global hotkeys
        private GlobalKeyboardHook _gHook;
        private string _pattern = @"((162)+(160)+(112|113|114))|((160)+(162)+(112|113|114|115))";
        readonly StringBuilder _stringBuilder = new StringBuilder();

        public bool IsLockUi
        {
            get { return _isLockUi; }
            set
            {
                _isLockUi = value;
                NotifyOfPropertyChange(() => IsLockUi);
            }
        }

        static Stopwatch _durationSw;
        static Stopwatch _reqPerSecSw;
        static Stopwatch _responseTimer;

        private int _clock;
        private List<double> _responses;

        public PlotViewModel PlotViewModel { get; set; }

        private CancellationTokenSource _cts;

        private int _success;
        private int _fail;
        private int _didNotHaveTimeToWork;

        private readonly object _mySuccessLock;
        private readonly object _myFailLock;
        private readonly object _myClockLock;

        private StringBuilder _infoConsoleStringBuilder;

        public MainViewModel()
        {
            _durationSw = new Stopwatch();
            _reqPerSecSw = new Stopwatch();
            _responseTimer = new Stopwatch();

            _mySuccessLock = new object();
            _myFailLock = new object();
            _myClockLock = new object();
            _requestTimeOut = new TimeSpan(0, 0, 0, 0, 1000);
            _infoConsoleStringBuilder = new StringBuilder();
            PlotViewModel = new PlotViewModel();

            GlovalHotkeys();
        }

        public string InfoConsole
        {
            get { return _infoConsoleStringBuilder.ToString(); }
            set
            {
                _infoConsoleStringBuilder.AppendLine(value);
                NotifyOfPropertyChange(() => InfoConsole);
            }
        }

        public int Clock
        {
            get { return _clock; }
            set
            {
                _clock = value;
                NotifyOfPropertyChange(() => Clock);
            }
        }

        public async Task StartTest()
        {
            IsLockUi = false;
            _responses = new List<double>();
            PlotViewModel.ClearPlot();
            _infoConsoleStringBuilder = new StringBuilder();
            _success = 0;
            _fail = 0;
            Clock = 0;

            _cts = new CancellationTokenSource();
            _durationSw.Restart();

            var results = new List<Task>();

            for (int i = 0; i < ThreadCount; i++)
            {
                Task task = Task.Run(async () => await MainRequestAsync(_cts.Token), _cts.Token);
                results.Add(task);
                InfoConsole = "Task " + i + " have Id = " + task.Id;
            }

            try
            {
                await Task.WhenAll(results);
                _didNotHaveTimeToWork = DurationSec * ThreadCount * RequestPerSec - _success - _fail;
                CompleteMessage();
            }
            catch (Exception e)
            {
                InfoConsole = e.Message;
                MessageBox.Show(e.Message);
            }
            finally
            {
                Clock = 0;
                IsLockUi = true;
            }
        }

        private void CompleteMessage()
        {
            InfoConsole = "Test complete! ";
            InfoConsole = "_______________________________________________________________________";
            InfoConsole = "Success: " + _success;
            InfoConsole = "Fail: " + _fail;
            InfoConsole = "Did not have time to work in one second: " + _didNotHaveTimeToWork;
            InfoConsole = "_______________________________________________________________________";
            InfoConsole = "Average response time: " + _responses.Average();
            InfoConsole = "Min response time: " + _responses.Min();
            InfoConsole = "Max response time: " + _responses.Max();
        }

        public async Task MainRequestAsync(CancellationToken cancellationToken)
        {
            try
            {
                while (_durationSw.ElapsedMilliseconds <= DurationSec * 1000)
                {

                    if (cancellationToken.IsCancellationRequested)
                    {
                        throw new OperationCanceledException();
                    }

                    _reqPerSecSw.Restart();
                    var requestsDone = 0;

                    while (_reqPerSecSw.ElapsedMilliseconds <= 1000)
                    {
                        if (requestsDone < RequestPerSec)
                        {
                            Request();
                            requestsDone++;
                        }
                    }
                    lock (_myClockLock)
                    {
                        Clock = (int)_durationSw.ElapsedMilliseconds / 1000;
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Request()
        {
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.Timeout = _requestTimeOut;
                    _responseTimer.Restart();
                    var result = httpClient.GetAsync(Url).Result;
                    _responseTimer.Stop();

                    if (result.IsSuccessStatusCode)
                    {
                        lock (_mySuccessLock)
                        {
                            InfoConsole = "Thread " + Thread.CurrentThread.ManagedThreadId + " send request." +
                                          " Status = " + (int) result.StatusCode + " " + result.StatusCode;
                            _responses.Add(_responseTimer.ElapsedMilliseconds);
                            PlotViewModel.AddPoint(_responseTimer.ElapsedMilliseconds);
                            _success++;
                        }
                    }
                    else
                    {
                        lock (_myFailLock)
                        {
                            _fail++;
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                lock (_myFailLock)
                {
                    _fail++;
                }
                if (ex.Response == null)
                    throw;
            }
            catch (AggregateException)
            {
                lock (_myFailLock)
                {
                    _fail++;
                    InfoConsole = "Thread " + Thread.CurrentThread.ManagedThreadId + " send request." + " Status = time out error";
                }
            }
        }

        public void CancelTest()
        {
            _cts?.Cancel();
            IsLockUi = true;
        }

        private void GlovalHotkeys()
        {
            _gHook = new GlobalKeyboardHook();
            _gHook.KeyDown += gHook_KeyDown;
            foreach (Keys key in Enum.GetValues(typeof(Keys)))
                _gHook.HookedKeys.Add(key);

            _gHook.Hook();
        }

        public async void gHook_KeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            _stringBuilder.Append(keyEventArgs.KeyValue.ToString());

            var hotkeyFound = Regex.IsMatch(_stringBuilder.ToString(), _pattern, RegexOptions.IgnoreCase);
            if (hotkeyFound)
            {
                var buttonName = int.Parse(_stringBuilder.ToString().Last().ToString()) - 1;
                _stringBuilder.Clear();

                switch (buttonName)
                {
                    case 1:
                        await StartTest();
                        break;
                    case 2:
                        CancelTest();
                        break;
                    case 3:
                        Quit();
                        break;
                }
            }
            if (_stringBuilder.Length == 1000000)
            {
                _stringBuilder.Clear();
            }
        }

        public void Quit()
        {
            Application.Current.Shutdown();
        }
    }
}