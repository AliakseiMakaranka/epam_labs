﻿using Caliburn.Micro;
using OxyPlot;

namespace UrlPerformance_Tester.ViewModels
{
    public class PlotViewModel : PropertyChangedBase
    {
        private int _positionX;
        private int _plotStep = 10;
        private readonly object _plotLock = new object();

        public string Title { get; }
        public BindableCollection<DataPoint> Points { get; }

        public PlotViewModel()
        {
            Title = "Response time";
            Points = new BindableCollection<DataPoint>();
        }

        public void AddPoint(double value)
        {
            lock (_plotLock)
            {
                _positionX += _plotStep;
                Points.Add(new DataPoint(_positionX, value));
            }
        }

        public void ClearPlot()
        {
            _positionX = 0;
            Points.Clear();
        }
    }
}
